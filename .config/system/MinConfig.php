<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Конфигурационный файл скрипта сжатия
* ***************************************************************** */

if (!defined('_MLWEB')) die('Доступ запрещен!');

return [
  /* ******************************** *
    *  Общий JS
  * ********************************* */

  "common_js" => [
    ASSETS_DIR . "/scripts/libs/jquery.min.js",          // Есты ты пидор, то удали этот файл
    ASSETS_DIR . "/scripts/libs/popper.min.js",          // Есты ты пидор, то удали этот файл

    ASSETS_DIR . "/scripts/libs/jquery.pjax.js",         // Библиотека AJAX
    ASSETS_DIR . "/scripts/libs/noty.js",                // Уведомления
    // ASSETS_DIR . "/scripts/libs/jTippy.js",              // ToolTip : https://github.com/HTMLGuyLLC/jTippy
    // ASSETS_DIR . "/scripts/libs/protip.js",              // ToolTip : http://protip.rocks/

    // ASSETS_DIR . "/fonts/FontAwesome/js/all.js",         // Шрифты

    ASSETS_DIR . "/scripts/src/minilife_libs.js",        // Труды пользователей StackOverFlow (шутка)
    ASSETS_DIR . "/scripts/src/ajax.js",                 // Все, что связано с AJAX

    ASSETS_DIR . "/scripts/src/dropdown.js",                 //

    // ASSETS_DIR . "/scripts/src/notifications.js",     // Уведомления
  ],

  /* ******************************** *
    *  Общий CSS
  * ********************************* */

  "common_css" => [
    ASSETS_DIR . "/fonts/FontAwesome/css/all.css",         // Шрифты

    ASSETS_DIR . "/styles/css/noty.css",                     // Уведомления
    // ASSETS_DIR . "/styles/css/jTippy.css",
    // ASSETS_DIR . "/styles/css/protip.css",
    ASSETS_DIR . "/styles/noty-themes/default.css",          // Тема уведомления
  ],

  /* ************************************************ */

  /* ******************************** *
    *  Основной JS
  * ********************************* */

  "main_js" => [
    TEMPLATE_DIR . "/scripts/main.js",
    TEMPLATE_DIR . "/scripts/preloader.js",
    TEMPLATE_DIR . "/scripts/ripple.js",
  ],

  /* ******************************** *
    *  Основной CSS
  * ********************************* */

  "main_css" => [
    TEMPLATE_DIR . "/styles/css/init.css",
  ],

];
