<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Настройки для URLHandler и ЧПУ
* ***************************************************************** */

if (!defined('_MLWEB')) die('Доступ запрещен!');

return [
  '/' => [                      // Ссылка
      'url' => [                // Параметры
          'action' => 'home',
      ],
      'type' => 'home',         // Тип страницы
      'handler' => '',          // Обработчик
      'hard' => false,          // Открытие без отображения интерфейса сайта
      'system' => false,        // Игнорирование оффлайн-режима
  ],

  '/login' => [
      'url' => [
          'action' => 'login',
      ],
      'type' => 'auth',
      'handler' => HANDLERS_DIR . '/Auth.php',
      'hard' => false,
      'system' => false,
  ],

  '/lostpassword' => [
      'url' => [
          'action' => 'lostpassword',
      ],
      'type' => 'auth',
      'handler' => HANDLERS_DIR . '/Auth.php',
      'hard' => false,
      'system' => false,
  ],

  '/register' => [
      'url' => [
          'action' => 'register',
      ],
      'type' => 'auth',
      'handler' => HANDLERS_DIR . '/Register.php',
      'hard' => false,
      'system' => false,
  ],

  '/news/(.*)' => [
      'url' => [
          'action' => 'news',
          'page' => '%1',
      ],
      'type' => 'news',
      'handler' => HANDLERS_DIR . '/News.php',
      'hard' => false,
      'system' => false,
  ],

  '/page/(.*)' => [
      'url' => [
          'action' => 'page',
          'page' => '%1',
      ],
      'type' => 'pages',
      'handler' => '',
      'hard' => false,
      'system' => false,
  ],

  /* ******************************** *
    *  Системное
  * ********************************* */

  '/debug' => [
      'url' => [
          'action' => 'debug',
      ],
      'type' => 'debug',
      'handler' => HANDLERS_DIR . '/Debug.php',
      'hard' => false,
      'system' => true,
  ],

  '/api/(.*)||/api' => [
      'url' => [
          'action' => 'api',
          'method' => '%1',
      ],
      'type' => 'api',
      'handler' => API_DIR . '/init.php',
      'hard' => true,
      'system' => true,
  ],

  '/min' => [
      'url' => [
          'action' => 'min',
      ],
      'type' => 'min',
      'handler' => MODULES_DIR . '/min/index.php',
      'hard' => true,
      'system' => true,
  ],
];