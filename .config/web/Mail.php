<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Конфигурация системных Email адресов
* ***************************************************************** */

if (!defined('_MLWEB')) die('Доступ запрещен!');

return [
  'noreply' => [
    'smtp_mail'   => 'noreply@minilife.su',
    'smtp_pass'   => '',
    'smtp_host'   => 'smtp.yandex.ru',
    'smtp_port'   => 465,
    'smtp_secure' => 'ssl',
  ],

  'support' => [
    'smtp_mail'   => 'support@minilife.su',
    'smtp_pass'   => '',
    'smtp_host'   => 'smtp.yandex.ru',
    'smtp_port'   => 465,
    'smtp_secure' => 'ssl',
  ],

];
