<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Гланый конфигурационный файл
* **************************************************************** */

if (!defined('_MLWEB')) die('Доступ запрещен!');

return [
  'name' => 'MiniLife',
  'title__ru_RU' => 'MiniLife - Твоя маленькая жизнь...',
  'title__en_US' => 'MiniLife - Your little life...',
  'title_add' => ' » ',
  'title_mode' => 2,
  'description__ru_RU' => 'Описание',
  'description__en_US' => '',
  'keywords__ru_RU' => 'Тута keywords',
  'keywords__en_US' => '',
  'theme' => 'minilife',
  'langs' => [
    'ru',
    'en'
  ],
  'timezone' => 'Europe/Moscow',
  'debug' => 1,
  'offline_mode' => '',
  'offline_title' => 'Title',
  'offline_message' => 'Message',
  'offline_pageTitle' => 'Page title',
  'enable_preloader' => ''
];