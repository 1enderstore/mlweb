<!DOCTYPE html>
<html lang="<?=ml_lang() ?>">
<head>
  <meta charset="utf-8">
  <title><?=ml_title() ?></title>

  <script type="text/javascript" src="/assets/scripts/libs/jquery.min.js"></script>
</head>
<body>
  <div style="margin-top: 30px; margin-left: 10px">
    <b style="color: #f91616">Debug info:</b><br>
    <div style="margin-left: 15px" id="debug-data"><?=Debug::output() ?></div>
  </div>

  <script>
    getDebugInfo();
    setInterval(function() {
      getDebugInfo();
    }, 2000);

    function getDebugInfo() {
      $.ajax({
        url: '/debug.txt',

        success: function($data) {
          $("#debug-data").html($data);
        }
      });
    }
  </script>
</body>
</html>
