<!DOCTYPE html>
<html lang="<?=ml_lang();?>">
  <head>
    <meta charset="utf-8">
    <title><?=ml_title();?></title>
    <meta name="description" content="<?=ml_description();?>">
    <meta name="keywords" content="<?=ml_keywords();?>">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <script type="text/javascript">var timerStart = Date.now();</script>

    <!-- Шрифты -->

    <!-- <link href="//assets.minilife.su/fonts/FontAwesome/css/all.min.css" rel="stylesheet" /> -->
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Exo:300,400,500,600,700|Comfortaa:300,400,500,600,700&display=swap&subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="180x180" href="<?=getPath('assets', 1);?>/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=getPath('assets', 1);?>/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=getPath('assets', 1);?>/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?=getPath('assets', 1);?>/images/favicon/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <link rel="stylesheet" type="text/css" href="/min?g=common_css&v=7">
    <link rel="stylesheet" type="text/css" href="/min?g=main_css&v=7">

    <script type="text/javascript">
      var _data = <?=ml_JSData();?>;
    </script>

    <script type="text/javascript" src="/min?g=common_js&v=7"></script>
    <!-- <script src="<?=getPath('assets', 1);?>/scripts/src/dropdown.js"></script> -->

    <script>autoLoader();</script>

<!--  -->

<!-- <link rel="stylesheet" href="<?=getPath('template', 1);?>/scripts/sweet-dropdown/min/jquery.sweet-dropdown.min.css" /> -->
<!-- <script src="<?=getPath('template', 1);?>/scripts/sweet-dropdown/dev/jquery.sweet-dropdown.js"></script> -->

<!-- <link rel="stylesheet" href="<?=getPath('template', 1);?>/scripts/mega-dropdown-master/css/style.css"> -->
<!-- <script src="<?=getPath('template', 1);?>/scripts/mega-dropdown-master/js/jquery.menu-aim.js"></script>
<script src="<?=getPath('template', 1);?>/scripts/mega-dropdown-master/js/modernizr.js"></script>
<script src="<?=getPath('template', 1);?>/scripts/mega-dropdown-master/js/main.js"></script> -->

<!--  -->

  </head>
  <body>
    <!-- <div class="line1"></div> -->
    {{include 'preloader.php'}}

    <?
      // d(offlinePlayerUuid('1EnderStore'));
    ?>

    <header class="navbar-header navbar-header-transparent">
        <a class="logo" <? if ($_SERVER['REQUEST_URI'] !== '/') echo 'href="/"' ?>></a>
        <nav class="main_menu">
          <ul>
            <li>
              <span><a <? if ($_SERVER['REQUEST_URI'] !== '/') echo 'href="/"' ?>>Главная</a></span>
            </li>

            <li>
              <span><a href="/">Форум</a></span>
            </li>

            <!-- <li class="dropdown">
              <span><a id="menu-toggle1" 1href="#">Наши серверы</a></span>
              <ul class="dropdown-menu" data-menu data-dropdown-toggle="#menu-toggle1">
                <li><a href="#">Item 1</a>></li>
                <li><a href="#">Item 2</a></li>
                <li class="divider"></li>
                <li><a href="#">Item 3</a></li>
              </ul>
            </li> -->


            <li class="dropdown">
              <span><a href="#">Наши серверы</a></span>
              <div class="dropdown-menu">
                <a href="#">Enigmatic</a>
                <a href="#">Neoteric</a>
              </div>
            </li>


<script>$('.dropdown').dropdown();</script>
<!-- <script>$('[data-menu]').dropdown();</script> -->

            <li class="dropdown">
              <span><a href="#">Поддержка</a></span>
              <!-- <div class="dropdown-menu show">
                <a href="#">Enigmatic</a>
                <a href="#">Neoteric</a>
              </div> -->
            </li>

            <li>
              <span class="dropdown"><a href="#">Правила</a></span>
              <div class="dropdown-menu">
                <a href="#">Соглашение</a>
                <a href="#">Игровые правила</a>
                <a href="#">Правила форума</a>
                <a href="#">Правила персонала</a>
              </div>
            </li>



            <li>
              <span><a href="#">Услуги</a></span>
            </li>
          </ul>
        </nav>

        <div class="right-block <? if (User::isAuth()) { ?>user-profile<? } ?>">
          <? if (User::isAuth()) { ?>
            <div class="avatar"><a href="#"><img src="<?=User::getUserData()['avatar'] ?>"></a></div>
            <div class="nickname dropdown" data-dropdown-toggle="dropdown">
              <a href="#"><?=User::getUserData()['username'] ?></a>
              <div class="dropdown-menu big">
                <a class="dropdown-item" href="#" onclick="unAuth();><span class="icon circle">4</span><?=LNG::getText('auth__exit');?></a>
              </div>
            </div>
          <? } else { ?>
            <ul class="ul-buttons">
            <li><a href="/login"><i class="fas fa-sign-in-alt"></i>Авторизация</a></li>
            <li><a href="/register"><i class="fas fa-user-plus"></i>Регистрация</a></li>
          </ul>
          <? } ?>


      </div>
      <div class="container dropdown-menu-big show" style="display: none">
        <div class="cards">

          <div class="card">
            <div class="image"><img src="/uploads/images/servers/neoteric/image.png"></div>
            <div class="card-body">
              <h4 class="title">Neoteric</h4>
              <p class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat quia incidunt sequi illum, nisi voluptatibus similique cum error maiores iste.</p>
              <div class="tags">
                <span class="badge pill b-green">IC2</span>
                <span class="badge pill b-purple">ThaumCraft</span>
                <span class="badge pill b-orange">ThermalMods</span>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="image"><img src="/uploads/images/servers/neoteric/image.png"></div>
            <div class="card-body">
              <h4 class="title">Neoteric</h4>
              <p class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat quia incidunt sequi illum, nisi voluptatibus similique cum error maiores iste.</p>
              <div class="tags">
                <span class="badge pill b-green">IC2</span>
                <span class="badge pill b-purple">ThaumCraft</span>
                <span class="badge pill b-orange">ThermalMods</span>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="image"><img src="/uploads/images/servers/neoteric/image.png"></div>
            <div class="card-body">
              <h4 class="title">Neoteric</h4>
              <p class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat quia incidunt sequi illum, nisi voluptatibus similique cum error maiores iste.</p>
              <div class="tags">
                <span class="badge pill b-green">IC2</span>
                <span class="badge pill b-purple">ThaumCraft</span>
                <span class="badge pill b-orange">ThermalMods</span>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="image"><img src="/uploads/images/servers/neoteric/image.png"></div>
            <div class="card-body">
              <h4 class="title">Neoteric</h4>
              <p class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat quia incidunt sequi illum, nisi voluptatibus similique cum error maiores iste.</p>
              <div class="tags">
                <span class="badge pill b-green">IC2</span>
                <span class="badge pill b-purple">ThaumCraft</span>
                <span class="badge pill b-orange">ThermalMods</span>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="image"><img src="/uploads/images/servers/neoteric/image.png"></div>
            <div class="card-body">
              <h4 class="title">Neoteric</h4>
              <p class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat quia incidunt sequi illum, nisi voluptatibus similique cum error maiores iste.</p>
              <div class="tags">
                <span class="badge pill b-green">IC2</span>
                <span class="badge pill b-purple">ThaumCraft</span>
                <span class="badge pill b-orange">ThermalMods</span>
              </div>
            </div>
          </div>

        </div>



      </div>
    </header>

    <div id="main">
      <!-- SLIDER -->
      <div class="slider_box">
        <div class="review">
          <div class="content no_select">
            <div class="title">О сервере Enigmatic</div>
            <div class="description">Ну тут должно быть мини-описание. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Provident dicta vitae quas dignissimos. Nostrum cupiditate perspiciatis tempora, reprehenderit incidunt nihil!
            </div>
          </div>
          <div class="background">
            <!-- <div class="image" style="background-image: linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3)), url(/uploads/posts/123/review.png);"></div> -->
            <div class="image" style="background-image: url('/uploads/pages/servers/enigmatic/review.png');"></div>
            <!-- <img src="/uploads/posts/123/review.png"> -->
          </div>
        </div>
      </div>
      <!-- /SLIDER -->

      <!-- BREADCRUMB -->
      <div class="breadcrumb no_select">
        <ul>
          <? if ($_SERVER['REQUEST_URI'] === '/') { ?>
          <li><i class="fas fa-newspaper"></i>Последние новости</li>
          <? } else echo Template::getBreadcrumbs() ?>

          <!-- <a class="button green dropdown" href="#" target="_blank">Подробнее</a> -->
          <!-- <li><a href="/"><i class="fas fa-home"></i>Главная</a></li>
          <li><a href="/"><i class="fas fa-server"></i>Серверы</a></li>
          <li><a href="/"><i class="fas fa-flask-potion"></i>Enigmatic</a></li> -->
        </ul>
      </div>
      <!-- /BREADCRUMB -->

      <div class="wrapper">
        <!-- CONTENT -->
        <div class="page-content">
          {{$content}}
          <!-- <div class="news_list">
            <a class="button circle back no-active">Предыдущая</a>
            <div class="pages-list">
              <a href="#">1</a>
              <span class="current">2</span>
              <a href="#">3</a>
              <a href="#">4</a>
              <a href="#">5</a>
              <a href="#">6</a>
              <a href="#">7</a>
              <a href="#">8</a>
              <span class="list"></span>
              <a href="#">19</a>
            </div>
            <a class="button circle next">Следующая</a>

          </div> -->

          <!-- <div style="height:10px;margin-top:400px;width:100%"></div>

          <div style="margin-bottom:50px">
            <a class="button primary">primary</a>
            <a class="button info">info</a>
            <a class="button success">success</a>
            <a class="button warning">warning</a>
            <a class="button danger">danger</a>
            <a class="button default">default</a>
          </div>

          <div style="width: 200px; margin-bottom:50px">
            <span class="badge pill b-blue">blue</span>
            <span class="badge pill b-green">green</span>
            <span class="badge pill b-orange">orange</span>
            <span class="badge pill b-pink">pink</span>
            <span class="badge pill b-purple">purple</span>
            <span class="badge pill b-red">red</span>
            <span class="badge pill b-yellow">yellow</span>
          </div>

          <div style="width: 200px; margin-bottom:100px">
            <span class="badge b-blue">blue</span>
            <span class="badge b-green">green</span>
            <span class="badge b-orange">orange</span>
            <span class="badge b-pink">pink</span>
            <span class="badge b-purple">purple</span>
            <span class="badge b-red">red</span>
            <span class="badge b-yellow">yellow</span>
          </div>

          <div class="cards">
            <div class="card">
              <div class="image"><img src="/uploads/images/servers/neoteric/image.png"></div>
              <div class="card-body">
                <h4 class="title+">IaDolbaiob</h4>
                <h6>Разработчик</h6>
                <p class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat quia incidunt sequi illum, nisi voluptatibus similique cum error maiores iste.</p>
                <div class="tags">
                  <span class="badge pill b-green">IC2</span>
                  <span class="badge pill b-purple">ThaumCraft</span>
                  <span class="badge pill b-orange">ThermalMods</span>
                </div>
              </div>
            </div>
          </div> -->

        </div>
        <!-- /CONTENT -->

        <!-- SIDEBLOCK -->
        <?=file_get_contents(getTemplatePath() . '/modules/sideblock.php') ?>
        <!-- /SIDEBLOCK -->



        <!-- <div class="content">
          <div class="news_block">
            asd
            <button class="button green">Подробнее<span class="icon arrow_right"></span></button>
            <a class="button green">Подробнее<i class="far fa-angle-right"></i></a>
          </div>
          <div class="news_block">
            <div class="head">
              <div class="profile">
                <div class="avatar"><a href="#"><img src="images/avatars/1EnderStore.jpg" /></a></div>
                <a href="#">1EnderStore</a>
              </div>
              <div class="stats">
                <div class="comments">3к</div>
                <div class="likes">9к</div>
              </div>
            </div>
            <div class="body">
              <div class="image">
                <span>EXAMPLE<br />TEXT</span>
                <a><img src="images/news/1.png"/></a>
              </div>
              <div class="content">
                <h2><a href="#">Lorem ipsum dolor sit amet.</a></h2>
                <div class="text">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde, sequi. Illum corrupti doloremque placeat. Officiis repellat numquam fugiat, quos perspiciatis odio dolorem? A rerum repellat corrupti maxime nulla aperiam impedit, deleniti rem. Quos dolore natus repudiandae, rerum et deleniti magni nobis incidunt praesentium possimus sit deserunt in, aperiam, nulla magnam.
                </div>
              </div>
              <button class="button green">Подробнее<span class="icon arrow_right"></span></button>
            </div>
          </div>
        </div> -->
        <!-- <div class="sidebar">
          <div class="side_block">
            <h2><i class="fas fa-seedling"></i>Привет. Ты тут впервые?</h2>

            <a class="button_l game">
              <div class="icon enigmatic"><i class="fas fa-gamepad"></i></div>
              <p>Начать играть</p>
              <span>Всего три шага</span>
            </a>

            <a class="button_l lock">
              <div class="icon"><i class="fas fa-lock-alt"></i></div>
              <p>Уже играешь?</p>
              <span>Войти на сайт</span>
            </a>
          </div>

          <div class="side_block">
            <h2><i class="fal fa-chart-area"></i>Мониторинг</h2>

            <div class="server_block">
              <div class="icon"><i class="fas fa-flask-potion enigmatic"></i></div>
              <div class="box">
                <div class="name">Enigmatic</div>
                <div class="status">64/100</div>
                <div class="bar"><div class="fill green" style="width: 64%;"></div></div>
              </div>
              <a class="info"></a>
            </div>

            <div class="server_block">
              <div class="icon"><i class="fas fa-cogs neoteric"></i></div>
              <div class="box">
                <div class="name">Neoteric</div>
                <div class="status">38/100</div>
                <div class="bar"><div class="fill blue" style="width: 38%;"></div></div>
              </div>
              <a class="info"></a>
            </div>

            <div class="server_block">
              <div class="icon"><i class="far fa-bow-arrow aeternum"></i></div>
              <div class="box">
                <div class="name">Aeternum</div>
                <div class="status error">недоступен</div>
                <div class="bar"><div class="fill red" style="width: 100%;"></div></div>
              </div>
              <a class="info"></a>
            </div>
          </div>
        </div> -->
      </div>
    </div>

    <div style="height:10px;margin-top:400px;width:100%"></div>

    <!-- <footer>
      &copy; 2015-2020 MiniLife.
    </footer> -->

    <script type="text/javascript" src="/min?g=main_js&v=7"></script>

    <script type="text/javascript">
      $(document).ready(function () {
        d("Time until DOMready: " + (Date.now() - timerStart) + 'ms', 'info', false);
      });
      $(window).load(function () {
        d("Time until everything loaded: " + (Date.now() - timerStart) + 'ms', 'info', false);
      });
    </script>

  </body>
</html>
