<div class="sideblock">

  <div class="section">
    <h4 class="title"><i class="fas fa-seedling"></i>Привет, ты тут впервые?</h4>
    <a href="#">
      <i class="fas fa-seedling"></i>
      <span>Начать играть</span>
      <p>Всего три шага</p>
    </a>
  </div>

  <div class="section">
    <h4 class="title"><i class="fas fa-chart-area"></i>Мониторинг</h4>
    <div class="monitoring-list">

      <div class="server-item">
        <div class="server-icon"><i class="fas fa-flask-potion enigmatic"></i></div>
        <div class="content">
          <span class="server-name">Enigmatic</span>
          <span class="server-status">63/100</span>

          <div class="server-status-bar">
            <div class="server-status-bar-filler progress-bar orange" style="width: 63%;"></div>
          </div>
        </div>
        <a class="server-link" href="#"></a>
      </div>

      <div class="server-item">
        <div class="server-icon"><i class="fas fa-cogs neoteric"></i></div>
        <div class="content">
          <span class="server-name">Neoteric</span>
          <span class="server-status">32/100</span>
          <div class="server-status-bar">
            <div class="server-status-bar-filler progress-bar green" style="width: 32%;"></div>
          </div>
        </div>
        <a class="server-link" href="#"></a>
      </div>

      <div class="server-item">
        <div class="server-icon"><i class="fas fa-bow-arrow aeternum"></i></div>
        <div class="content">
          <span class="server-name">Aeternum</span>
          <span class="server-status">12/100</span>
          <div class="server-status-bar">
            <div class="server-status-bar-filler progress-bar blue" style="width: 12%;"></div>
          </div>
        </div>
        <a class="server-link" href="#"></a>
      </div>

      <div class="server-item">
        <div class="server-icon"><i class="fas fa-hammer"></i></div>
        <div class="content">
          <span class="server-name">Test Server</span>
          <span class="server-status">51/100</span>
          <div class="server-status-bar">
            <div class="server-status-bar-filler progress-bar yellow" style="width: 51%;"></div>
          </div>
        </div>
        <a class="server-link" href="#"></a>
      </div>

      <div class="server-item">
        <div class="server-icon"><i class="fas fa-cog"></i></div>
        <div class="content">
          <span class="server-name">Test Server 2</span>
          <span class="server-status">недоступен</span>
          <div class="server-status-bar">
            <div class="server-status-bar-filler progress-bar red" style="width: 100%;"></div>
          </div>
        </div>
        <a class="server-link" href="#"></a>
      </div>

    </div>
  </div>

</div>