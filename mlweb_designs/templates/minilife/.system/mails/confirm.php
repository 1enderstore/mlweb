<table width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;background-color:#f5f8fa;margin:0;padding:0;width:100%;">
   <tbody>
      <tr>
         <td align="center" style="font-family:Avenir,Helvetica,sans-serif;">
            <table width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;margin:0;padding:0;width:100%;">
               <tbody>
                  <tr>
                     <td style="font-family:Avenir,Helvetica,sans-serif;padding:25px 0;text-align:center;">
                        <a href="https://mcminilife.ru" style="font-family:Avenir,Helvetica,sans-serif;color:#bbbfc3;font-size:19px;font-weight:bold;text-decoration:none;"
                           class="daria-goto-anchor" target="_blank" rel="noopener noreferrer">
                           MiniLife
                        </a>
                     </td>
                  </tr>
                  <tr>
                     <td width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;background-color:#FFFFFF;border-bottom:1px solid #EDEFF2;border-top:1px solid #EDEFF2;margin:0;padding:0;width:100%;">
                        <table align="center" width="570" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;background-color:#FFFFFF;margin:0 auto;padding:0;width:570px;">
                           <tbody>
                              <tr>
                                 <td style="font-family:Avenir,Helvetica,sans-serif;padding:35px;">
                                    <h1 style="font-family:Avenir,Helvetica,sans-serif;color:#2F3133;font-size:19px;font-weight:bold;margin-top:0;text-align:left;">Здравствуйте, {%username%}!</h1>
                                    <p style="font-family:Avenir,Helvetica,sans-serif;color:#74787E;font-size:16px;line-height:1.5em;margin-top:0;text-align:left;">
                                       Вы получили это письмо, так как этот адрес электронной почты был использован при регистрации на нашем ресурсе. Аккаунт будет создан сразу после того, как Вы подтвердите регистрацию.
                                       <br />
                                       Если это были вы, пожалуйста, проигнорируйте это письмо.
                                    </p>
                                    <table align="center" width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;margin:30px auto;padding:0;text-align:center;width:100%;">
                                       <tbody>
                                          <tr>
                                             <td align="center" style="font-family:Avenir,Helvetica,sans-serif;">
                                                <table border="0" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;">
                                                   <tbody>
                                                      <tr>
                                                         <td style="font-family:Avenir,Helvetica,sans-serif;">
                                                            <a href="{%validationlink%}" target="_blank" style="font-family:Avenir,Helvetica,sans-serif;border-radius:3px;color:#FFF;display:inline-block;text-decoration:none;background-color:#6CB200;border-top:10px solid #6CB200;border-right:18px solid #6CB200;border-bottom:10px solid #6CB200;border-left:18px solid #6CB200;"
                                                               rel="noopener noreferrer">Активировать аккаунт</a>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                   <p style="font-family:Avenir,Helvetica,sans-serif;color:#74787E;font-size:16px;line-height:1.5em;margin-top:10;text-align:left;">
                                       Ваши данные:<br />
                                       Никнейм: {%username%}<br />
                                       Пароль: {%password%}<br />
                                    </p>

                                    <p style="font-family:Avenir,Helvetica,sans-serif;color:#74787E;font-size:16px;line-height:1.5em;margin-top:0;text-align:left;">С уважением,<br>MiniLife</p>
                                    <table width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;border-top:1px solid #EDEFF2;margin-top:25px;padding-top:25px;">
                                       <tbody>
                                          <tr>
                                             <td style="font-family:Avenir,Helvetica,sans-serif;">
                                                <p style="font-family:Avenir,Helvetica,sans-serif;color:#74787E;line-height:1.5em;margin-top:0;text-align:left;font-size:12px;">Если вы не можете нажать на "Активировать аккаунт" кнопку, скопируйте и вставьте эту ссылку:
                                                   <a href="" style="font-family:Avenir,Helvetica,sans-serif;color:#6CB200;"
                                                       target="_blank" rel="noopener noreferrer"></a><a href="{%validationlink%}"
                                                      style="font-family:Avenir,Helvetica,sans-serif;color:#6CB200;" target="_blank" rel="noopener noreferrer"></a>{%validationlink%}</p>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td style="font-family:Avenir,Helvetica,sans-serif;">
                        <table align="center" width="570" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;margin:0 auto;padding:0;text-align:center;width:570px;">
                           <tbody>
                              <tr>
                                 <td align="center" style="font-family:Avenir,Helvetica,sans-serif;padding:35px;">
                                    <p style="font-family:Avenir,Helvetica,sans-serif;line-height:1.5em;margin-top:0;color:#AEAEAE;font-size:12px;text-align:center;">© 2020 MiniLife. Все права защищены.</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>