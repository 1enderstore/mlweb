<form id="login">
  <input type="text" name="username" placeholder="<?=LNG::getText('auth__username') ?>" required><br>
  <input type="password" name="password" placeholder="<?=LNG::getText('auth__password') ?>" required><br>
  <input type="checkbox" name="remember" value="yes"><?=LNG::getText('auth__remember') ?><br>
  <input type="submit" value="<?=LNG::getText('auth__login') ?>" onclick="">
</form>

<script type="text/javascript">
  $("#login").submit(function(e) {
    e.preventDefault();

    $.ajax({
      url: '/api/user.auth',
      type: 'GET',
      dataType: 'json',
      data: $(this).serialize() + "&auth_type=full",
    })
    .done(function($response) {
      if ($response['code'] == 1) {
        $.pjax.reload('#content');
        // reloadConent();
        new Noty({
          text: $response['response']['text'],
          message: $response['response']['message'],
          type: 'success',
        }).show();
      }
      else {
        new Noty({
          text: $response['response']['text'],
          message: $response['response']['message'],
          type: 'warning',
        }).show();
      }
    })
    .fail(function() {
      new Noty({
          text: $response['response']['text'],
          message: $response['response']['message'],
        type: 'error',
      }).show();
    });
  });
</script>