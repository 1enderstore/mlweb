/* ******************************** *
  *  Resize
* ********************************* */

// $(window).resize(function() {resize();});
// resize();

function resize() {
  var select = '[resize]';
  $('[resize]').each(function (e) {
    var $this = $(this);

    var tmp = $this.attr('style');
    // tmp = $('.sliders').attr('style');
    if (!tmp) {
      var tmp = [];
    } else {
      tmp = tmp.split(';');
      tmp = tmp.slice(0, -1);
    }

    var styles = {};
    $.each(tmp, function(idx,val) {
      var tmp2 = val.split(':');
      styles[tmp2[0]] = tmp2[1];
    });

    styles['height'] = ($(window).height() - 60) + 'px';

    var styles_ = [];
    $.each(styles, function(idx2,val2) {
      var tmp3 = idx2 + ":" + val2 + ';';
      styles_.push(tmp3);
    });

    var styles_ = styles_.join('');
    $($this).attr('style', styles_);
  });
}

/* ******************************** *
  *  Обработка ссылок
* ********************************* */

$("a[href*='#']").click(function() {
  // new Noty({
  //   text: 'Упс..',
  //   message: 'Кажется, эта кнопка не работает',
  //   type: 'warning',
  // }).show();
  return(false);
});

$(".no-active").click(function() {
  return(false);
});

/* ******************************** *
  *  DropDown Menu
* ********************************* */


// var dropdowns = $(".main_menu > ul > li.dropdown");
// dropdowns = $.merge(dropdowns, $(".user-profile > .nickname"));

// dropdowns.click(function(e) {
//   var $this = $(this);
//   var dropdown = $this.children(".dropdown_menu");

//   if (!dropdown.hasClass('visible')) {

//     dropdown.show();
//     setTimeout(function() {
//       dropdown.addClass('visible');
//       $this.addClass('selected');
//     }, 1);
//   } else {
//     dropdown.removeClass('visible');
//     $this.removeClass('selected');
//     var id = setTimeout(function() {
//       dropdown.hide();
//     }, 200);
//   }
// });

/* ******************************** *
  *  Navbar sccroll
* ********************************* */

window.addEventListener('scroll', function() {
  // document.getElementById('showScroll').innerHTML = pageYOffset + 'px';
  var $navbar = $('.navbar-header');
  var $navbar_transparent = 'navbar-header-transparent';

  if (window.scrollY >= 1) {
    $navbar.removeClass($navbar_transparent);
  } else {
    $navbar.addClass($navbar_transparent);
  }
});

/* ******************************** *
  *
* ********************************* */
