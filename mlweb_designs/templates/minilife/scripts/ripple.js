// $(".button").on("click", function (event) {
//   $(this).append("<span class='ripple-effect'>");
//   $(this).find(".ripple-effect").css({
//     left: event.pageX - $(this).position().left,
//     top: event.pageY - $(this).position().top
//   }).animate({
//     opacity: 0,
//   }, 1500, function () {
//     $(this).remove();
//   });
// });


$(".button").mousedown(function (e) {
  var posX = $(this).offset().left,
      posY = $(this).offset().top,
      buttonWidth = $(this).width(),
      buttonHeight =  $(this).height();


  $(this).prepend("<span class='ripple'></span>");

  if(buttonWidth >= buttonHeight) {
    buttonHeight = buttonWidth;
  } else {
    buttonWidth = buttonHeight;
  }

  $(this).find(".ripple").css({
    width: buttonWidth,
    height: buttonHeight,
    top: y = e.pageY - posY - buttonHeight / 2 + 'px',
    left: e.pageX - posX - buttonWidth / 2 + 'px',
  }).animate({
    opacity: 0,
  }, 1500, function () {
    $(this).remove();
  });
});