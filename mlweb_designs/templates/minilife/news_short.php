<article class="news-short">

  <div class="header no_select">
    <ul class="counters">
      <li class="likes">{{$likes_count}}</li>
      <li class="views">{{$views_count}}</li>
    </ul>

    <div class="user-profile">
      <div class="avatar"><a href="#"><img src="{{$author_avatar}}"></a></div>
      <div class="nickname"><a href="#">{{$author}}</a></div>
    </div>

  </div>

  <div class="body">
    <div class="left-block">
      <a href="{{$url_full}}">
        <div class="image">
          <!-- TODO: Прихуярить выбор среднего цвета для border изображения при hover
                    http://www.manhunter.ru/webmaster/1028_opredelenie_osnovnogo_cveta_izobrazheniya_na_php.html
                    https://yandex.ru/search/?text=php%20%D1%81%D1%80%D0%B5%D0%B4%D0%BD%D0%B8%D0%B9%20%D1%86%D0%B2%D0%B5%D1%82%20%D0%B8%D0%B7%D0%BE%D0%B1%D1%80%D0%B0%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F&&lr=216
          -->
          <img src="{{$image_url}}">
        </div>
      </a>
      <div class="date">{{$date}}</div>
    </div>
    <div class="content">
      <h3 class="title">
        <a href="{{$url_full}}">{{$title}}</a>
      </h3>
      <div class="text">{{$content}}</div>

  <div class="footer right">
    <!-- <a class="button green" href="{{$url_full}}">Подробнее</a> -->
    <a class="button green animate" href="{{$url_full}}" target="_blank">Подробнее</a>
  </div>
    </div>

  </div>


</article>
<div class="hr-margin"></div>