<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Подгрузка всей CMS
* ***************************************************************** */

define('_MLWEB', true);

/* ******************************** *
  *  Назначение глобальных переменных
* ********************************* */

define('ASSETS_DIR', ROOT_DIR . '/assets');

define('TEMP_DIR', ROOT_DIR . '/tmp');
define('LOGS_DIR', ROOT_DIR . '/.logs');
define('CACHE_DIR', ROOT_DIR . '/.cache');
define('UPLOADS_DIR', ROOT_DIR . '/uploads');

define('USERS_DIR', UPLOADS_DIR . '/users');


define('CONFIG_DIR', ROOT_DIR . '/.config');
// define('SESSIONS_DIR', ROOT_DIR . '/.sessions');

define('LANGUAGES_DIR', ASSETS_DIR . '/languages');

define('MODULES_DIR', CORE_DIR . '/modules');
define('HANDLERS_DIR', CORE_DIR . '/handlers');

define('API_DIR', MODULES_DIR . '/api');

define('LIBS_DIR', CORE_DIR . '/libs');
include_once LIBS_DIR . '/Functions.php';

define('CLASSES_DIR', CORE_DIR . '/classes');
include_once CLASSES_DIR . '/Debug.php';
include_once CLASSES_DIR . '/TimerExecute.php';
TimerExecute::start();
include_once CLASSES_DIR . '/Log.php';
include_once CLASSES_DIR . '/Configuration.php'; new CFG();
include_once CLASSES_DIR . '/MySQL.php'; new DB;
define('DB_PREFIX', CFG::get('db_prefix'));

include_once CLASSES_DIR . '/Localization.php'; new LNG;
// include_once CLASSES_DIR . '/Helper.php';

define('DESIGNS_DIR', ROOT_DIR . '/mlweb_designs');
define('TEMPLATES_DIR', DESIGNS_DIR . '/templates');
define('TEMPLATE', CFG::get('theme'));
define('TEMPLATE_DIR', TEMPLATES_DIR . '/' . TEMPLATE);
define('TEMPLATE_DIR_MIN', str_replace(ROOT_DIR, '', TEMPLATE_DIR));

/* ******************************** *
  *  Проверка и создание дерикторий
* ********************************* */

// Sessions
// if (!is_dir(SESSIONS_DIR) && !file_exists(SESSIONS_DIR))
//   @mkdir(SESSIONS_DIR, 0777);
// if(@fileperms(SESSIONS_DIR) != '0777') @chmod(SESSIONS_DIR, 0777);

// Cache
if (!is_dir(CACHE_DIR) && !file_exists(CACHE_DIR))
  @mkdir(CACHE_DIR, 0777);
// if(@fileperms(CACHE_DIR) != '0777') @chmod(CACHE_DIR, 0777);

if (!is_dir(CACHE_DIR . "/min") && !file_exists(CACHE_DIR . "/min"))
  @mkdir(CACHE_DIR . "/min", 0777);
// if(@fileperms(CACHE_DIR . "/min") != '0777') @chmod(CACHE_DIR . "/min", 0777);

// Uploads
if (!is_dir(UPLOADS_DIR) && !file_exists(UPLOADS_DIR))
  @mkdir(UPLOADS_DIR, 0777);
// if(@fileperms(UPLOADS_DIR) != '0777') @chmod(UPLOADS_DIR, 0777);

if (!is_dir(UPLOADS_DIR . "/files") && !file_exists(UPLOADS_DIR . "/files"))
  @mkdir(UPLOADS_DIR . "/files", 0777);
// if(@fileperms(UPLOADS_DIR . "/files") != '0777') @chmod(UPLOADS_DIR . "/files", 0777);

if (!is_dir(UPLOADS_DIR . "/images") && !file_exists(UPLOADS_DIR . "/images"))
  @mkdir(UPLOADS_DIR . "/images", 0777);
// if(@fileperms(UPLOADS_DIR . "/images") != '0777') @chmod(UPLOADS_DIR . "/images", 0777);

if (!is_dir(UPLOADS_DIR . "/posts") && !file_exists(UPLOADS_DIR . "/posts"))
  @mkdir(UPLOADS_DIR . "/posts", 0777);
// if(@fileperms(UPLOADS_DIR . "/posts") != '0777') @chmod(UPLOADS_DIR . "/posts", 0777);

if (!is_dir(UPLOADS_DIR . "/users") && !file_exists(UPLOADS_DIR . "/users"))
  @mkdir(UPLOADS_DIR . "/users", 0777);
// if(@fileperms(UPLOADS_DIR . "/posts") != '0777') @chmod(UPLOADS_DIR . "/posts", 0777);

// Logs
if (!is_dir(LOGS_DIR) && !file_exists(LOGS_DIR))
  @mkdir(LOGS_DIR, 0777);
// if(@fileperms(LOGS_DIR) != '0777') @chmod(LOGS_DIR, 0777);

/* ******************************** *
  *  Преднастройки
* ********************************* */

// PHP config
@ini_set('session.gc_maxlifetime', 2419200);
@ini_set('session.cookie_lifetime', 2419200);
@ini_set('session.name', "MINILIFE");
@ini_set('session.sid_length', 64);
// @ini_set('session.save_path', SESSIONS_DIR);

session_start();
date_default_timezone_set(CFG::get('timezone'));

/* ******************************** *
  *  Подключение библиотек
  *  и настроек
  *  // Порядок не нарушать!
* ********************************* */

include_once CLASSES_DIR . '/URLHandler.php';
new URLHandler;
include_once CLASSES_DIR . '/Template.php';
include_once CLASSES_DIR . '/Security.php';

include_once CLASSES_DIR . '/Users.php';
new User;

include_once CORE_DIR . '/engine.php';

print(PHP_EOL . '<!-- Load time: ' . TimerExecute::finish(3) . ' sec. -->');