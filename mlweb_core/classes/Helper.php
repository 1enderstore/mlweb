<?php
/* **************************************************************** *
 *  MiniLife - Your little life...
 *  Назначение:
 *    Сборник функций
 * ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class Helper {

  /* ******************************** *
   *  Проверка изображения
   * ********************************* */

  public static function checkImage($url) {
    return (@file_exists($url)) ? true : false;
  }

  /* ******************************** *
   *  Форматирование даты
   * ********************************* */

  public static function formatDate($date, $time = true) {
    $month = date("%m", $date);
    $currentDate = (int) date('d', $date) . date(' %m Y', $date);
    $currentDate = str_replace($month, LNG::getText('month_' . (int) substr($month, 1)), $currentDate);

    if ($time == true) {
      $currentDate .= date(' в H:i', $date);
    }

    return $currentDate;
  }

  /* ******************************** *
   *  Обрезать строку до N символа
   *  и добавить троеточие
   * ********************************* */

  public static function shorten($string, $length, $ending = '...') {
    // $string = substr($string, 0, $length);
    // $string = rtrim($string, "!,.-");
    // $string = substr($string, 0, strrpos($string, ' '));

    $len = strlen(trim($string));
    $string = (($len > $length) && ($len != 0)) ? rtrim(mb_strimwidth($string, 0, $length - strlen($ending))) . $ending : $string;
    return $string;
  }

  /* ******************************** *
   *  Форматирование HTML
   * ********************************* */

  public static function formatHTML($string) {
    $string = strip_tags($string);
    return $string;
  }

  /* ******************************** *
   *  Форматирование HTML
   * ********************************* */

  public static function getTemplateName() {
    return CFG::get('theme');
  }

  /* ******************************** *
   *  Форматирование HTML
   * ********************************* */

  public static function getTemplatePath() {
    return TEMPLATES_DIR . '/' . self::getTemplateName();
  }

  /* ******************************** *
   *  Форматирование HTML
   * ********************************* */

  public static function getTemplateURI() {
    return str_replace(ROOT_DIR, '', self::getTemplatePath());
  }

  /* ******************************** *
   *  Форматирование HTML
   * ********************************* */

  public static function getPath($dir, $relative = false) {
    $paths = [
      'assets' => ASSETS_DIR,
      'template' => TEMPLATES_DIR . '/' . self::getTemplateName(),
      'templates' => TEMPLATES_DIR,
    ];
    return ($relative == true) ? str_replace(ROOT_DIR, '', $paths[$dir]) : $paths[$dir];
  }

}