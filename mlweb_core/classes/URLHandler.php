<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    ЧПУ и обработка запросов
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class URLHandler {
  public static $data;
  public static $url;
  public static $url_;
  public static $params;

  public static $CONFIG_PATH;

  /* ******************************** *
    *  Преднастройки
  * ********************************* */

  function __construct() {
    self::$CONFIG_PATH = include CONFIG_DIR . '/system/URLConfig.php';

    self::$url  = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    self::$url_ = substr(self::$url, 1);

    self::$data = self::prepare();
    self::$params = self::$data['url'];
  }

  /* ******************************** *
    *  Обработка запросов
    *  (сопоставление с конфигом)
  * ********************************* */

  public static function prepare() {
    $data = '[404]';

    foreach (self::$CONFIG_PATH as $key => $value) {
      $preg = false;
      foreach (explode('||', $key) as $value_) {
        $preg = (preg_match('@^' . $value_ . '$@Uu', self::$url) == true);
        if ($preg == true) break;
      }
      if ($preg) {
        $data          = self::$CONFIG_PATH[$key];
        $deconstructed = self::deconstructUrl(self::$url);
        $i = 0;
        foreach ($data['url'] as $key => $value) {
          if (preg_match("/^%[0-9]{1,3}$/", $value)) {
            $i++;
            $data['url'][$key] = str_replace('%' . $i, $deconstructed['params'][$i - 1] ?? null, $data['url'][$key]);
            $data['url'][$key] = str_replace('%', '', $data['url'][$key]);
          }
        }
        break;
      }
    }

    return $data;
  }

  /* ******************************** *
    *  Возвращает все данные по ссылке
  * ********************************* */

  public static function getData() {
    return self::$data;
  }

  /* ******************************** *
    *  Определение типа запроса
    *  P.S. Мне лень вытягивать это из переменной
    *  и я решил сделать отдельную функцию
  * ********************************* */

  public static function getType() {
    return (self::$data['type']) ?? false;
  }

  /* ******************************** *
    *  Разбор ссылки на составляющие
  * ********************************* */

  public static function deconstructUrl($url) {
    $params = [];

    $url_exp  = substr($url, 1);
    $url_exp  = explode('/', $url_exp);
    $url_conv = $url_exp[0];
    if (isEmpty($url_conv)) $url_conv = "/";
    else {
      unset($url_exp[0]);
      foreach ($url_exp as $key => $value) {
        if (isEmpty($value)) break;
        $params[] = $value;
      }
    }
    $result = [
      'type'   => $url_conv,
      'params' => $params,
    ];
    return $result;
  }

}
