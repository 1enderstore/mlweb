<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Класс для работы с базой данных
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class DB {
  static private $db;
  static private $status;
  static private $file;

  /* ******************************** *
    *  Подключение к БД
  * ********************************* */

  function __construct() {
    try {
      $dsn      = "mysql:host=" . CFG::get('db_host') . ";dbname=" . CFG::get('db_name') . ";charset=utf8";
      $opt      = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
      ];
      self::$db = new PDO($dsn,
        CFG::get('db_user'),
        CFG::get('db_pass'),
        $opt
      );
    } catch (PDOException $e) {
      // LOG::add("error", LNG::getText("error_db"), basename(__FILE__, ".php"));
      die("<h3 style=\"color: red\">Ошибка соединения с БД.</h3><br>{$e->getMessage()}");
    }
  }

  private static function errorConnect() {

  }



  /**
   * Выполнить запрос
   */
  public static function query($sql) {
    return self::$db->query($sql);
  }

  /**
   * Выполнение запроса, не требующего возврата результата.
   * К нему относится тоже примечание, что и для DB::query.
   * Так же его всегда следует использовать вместо DB::query, если запрос НЕ возвращает результата (обычно это UPDATE или DELETE), так как он, как указано, работает быстрее.
   * Возвращает кол-во затронутых строк.
   */
  public static function exec($sql) {
    return self::$db->exec($sql);
  }

  /**
   * Первая вкусность, которая упрощает жизнь. Выбор только одного значения из БД, соотв. запросу.
   */
  public static function column($sql) {
    return self::$db->query($sql)->fetchColumn();
  }

  /**
   * Тоже самое, только возвращается intval значение поля выборки, что бывает полезно.
   */
  public static function columnInt($sql) {
    return intval(self::$db->query($sql)->fetchColumn());
  }

  /**
   * Подготовка запроса, возвращает PDOStatement.
   */
  public static function prepare($sql) {
    return self::$db->prepare($sql);
  }

  /**
   * Получение lastInsertId от последнего запроса INSERT.
   */
  public static function lastInsertId() {
    return self::$db->lastInsertId();
  }

  /**
   * Подготавливает и сразу выполняет запрос.
   * Каждый ? или :var в теле запроса должен соотв. значению из массива $ar.
   */
  public static function execute($sql, $ar) {
    return self::$db->prepare($sql)->execute($ar);
  }

  /**
   * Возвращает строку последней ошибки над DB вместе с кодами в скобках.
   * Помните, что при использования PDOSatement’ов ошибки, связанные именно с их исполнением, следует обрабатывать над ними, а не над общей DB.
   */
  public static function error() {
    $ar = self::$db->errorInfo();
    return $ar[2] . ' (' . $ar[1] . '/' . $ar[0] . ')';
  }

  /**
   * Возвращают результат выполнения $sql как PDO::FETCH_ASSOC.
   */
  public static function fetchAssoc($sql) {
    return self::$db->query($sql)->fetch(PDO::FETCH_ASSOC);
  }


  public static function fetchAll($sql) {
    return self::$db->query($sql)->fetchAll();
  }

  /**
   * Возвращают результат выполнения $sql как PDO::FETCH_NUM.
   */
  public static function fetchNum($sql) {
    return self::$db->query($sql)->fetch(PDO::FETCH_NUM);
  }


  public static function getData($table, $if = 1) {
    $query = "SELECT * FROM " . DB_PREFIX . "{$table} WHERE {$if}";
    return ($if === 1) ? DB::fetchAll($query) : DB::fetchAssoc($query);
  }
}