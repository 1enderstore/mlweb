<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Дебаг
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class Debug {
  public static $data = [];

  /* ******************************** *
    *  Добавление данных
  * ********************************* */

  public static function add($varname, $var) {
    self::$data[$varname] = $var;
  }

  /* ******************************** *
    *  Генерация вывода данных
  * ********************************* */

  public static function output() {
    return self::getList(self::$data);
  }

  /* ******************************** *
    *  Генерация дерева
  * ********************************* */

  public static function getList($array, $margin = 0) {
    $result = "";
    foreach ($array as $key => $value) {
      if ($value === null) $value = "<i>null</i>";
      // elseif ($value === underfined) $value = "<i>underfined</i>";
      elseif (is_array($value) && empty($value)) $value = "<i>empty</i>";

      if (is_array($value)) {
        $margin += 20;
        $result .= "<div><b>{$key}</b>: " . PHP_EOL;
        $result .= self::getList($array[$key], $margin);
        $result .= "</div>" . PHP_EOL;
        $margin -= 20;
      } else {
        if ($value === true) $value = "<i>true</i>";
        elseif ($value === false) $value = "<i>false</i>";
        elseif ($value === "") $value = "<i>no-data</i>";
        $result .= "<div style=\"margin-left: {$margin}px\"><b>{$key}</b>: {$value}</div>" . PHP_EOL;
      }
    }
    return $result;
  }

  /* ******************************** *
    *  Запись вывода дебага в файл
  * ********************************* */

  public static function write($file) {
    if (CFG::get('debug') != true) return false;

    file_put_contents($file, self::output());
    // $f = fopen($file, 'w+');
    // fwrite($f, self::output());
    // fclose($f);

  }

}
