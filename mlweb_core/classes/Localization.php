<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Модуль локализации
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class LNG {
  public static $lng = [];

  /* ******************************** *
    *  Автозагрузка языков
  * ********************************* */

  function __construct() {
    CFG::$config['langs'] = array_merge(['.default'], CFG::$config['langs']);

    foreach (CFG::get('langs') as $lang) {
      foreach (glob(LANGUAGES_DIR . "/{$lang}" . "/*.json") as $path) {
        self::includeLang($lang, $path);
      }
    }
  }

  /* ******************************** *
    *  Загрузить язык
  * ********************************* */

  public static function includeLang($lang, $path) {
    if (is_file($path)) {
      $tmp = (array)json_decode(file_get_contents($path), false, 4096, JSON_BIGINT_AS_STRING);
      if (!isset(self::$lng[$lang])) self::$lng[$lang] = [];
      self::$lng[$lang] = array_merge(self::$lng[$lang], $tmp);
    }
  }

  /* ******************************** *
    *  Определение языка
  * ********************************* */

  // public static function getLang() {
  //   preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]), $matches); // вычисляем соответствия с массивом $matches
  //   $langs = array_combine($matches[1], $matches[2]); // Создаём массив с ключами $matches[1] и значениями $matches[2]
  //   foreach ($langs as $n => $v)
  //     $langs[$n] = $v ? $v : 1; // Если нет q, то ставим значение 1
  //   arsort($langs);
  //   return array_keys($langs, 1)[0];
  // }

  public static function getLang() {
    preg_match('/^\w{2}/',$_SERVER['HTTP_ACCEPT_LANGUAGE'], $matches);

    switch (strtolower($matches[0])){
      case "ru":
        $accept_lang="ru";
        break;
      case "en":
      case "uk":
      case "us":
        $accept_lang="en";
        break;
      case "ua":
        $accept_lang="ua";
        break;

      default:
        $accept_lang="en";
        break;
    }

    return $accept_lang;
  }

  /* ******************************** *
    *  Вывод текста с
    *  автоопределением языка
  * ********************************* */

  public static function getText($name, $vars = [], $lang = 'auto') {
    if (isEmpty($name)) return false;

    $text = "";
    if ($lang == 'auto' OR isEmpty($lang)) $text = @self::$lng[self::getLang()][$name];
    else $text = @self::$lng[$lang][$name];

    if (isEmpty($text)) $text = @self::$lng['en'][$name];
    if (isEmpty($text)) $text = @self::$lng['.default']['translate_not_found'];
    else {
      if (count($vars) >= 1) {
        for ($i = 0; $i < count($vars); $i++) {
          $text = str_replace('%' . ($i + 1), $vars[$i], $text);
        }
      }
    }

    return $text;
  }

  /* ******************************** *
    *  Получить верстку с языком или
    *  с языками
  * ********************************* */

  public static function getTreeLang($lang = null) {
    if (isEmpty($lang)) $lang = self::getLang();

    if (is_array($lang)) {
      foreach ($lang as $key => $value) {
        $output[$value] = self::$lng[$value];
      }
    } else {
      $output[$lang] = self::$lng[$lang];
    }

    return $output;
  }

}
