<?php
/* **************************************************************** *
 *  MiniLife - Your little life...
 *  Назначение:
 *    Описание
 * ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

use xPaw\MinecraftPing;

include LIBS_DIR . '/PHP-Minecraft-Query/MinecraftPing.php';
include LIBS_DIR . '/PHP-Minecraft-Query/MinecraftPingException.php';

class MinecraftServerPinger {

  /* ******************************** *
   *  TEXT
   * ******************************** */

  function __construct() {

  }

  /* ******************************** *
   *  Парсинг сервера
   * ******************************** */

  public static function parseStatus($data) {
    if (is_array($data)) $address = $data['address'];
    else $address = $data;

    $tmp = explode(':', $address);
    if (count($tmp) == 2) {
      $ip = $tmp[0];
      $port = $tmp[1];
    } else {
      $ip = $tmp[0];
      $port = 25565;
    }
    unset($tmp);

    $info = false;
    $query = null;
    try {
      $query = new MinecraftPing($ip, $port);

      $info = $query->Query();

      if ($info === false) {
        /*
         * If this server is older than 1.7, we can try querying it again using older protocol
         * This function returns data in a different format, you will have to manually map
         * things yourself if you want to match 1.7's output
         *
         * If you know for sure that this server is using an older version,
         * you then can directly call QueryOldPre17 and avoid Query() and then reconnection part
         */

        $query->Close();
        $query->Connect();

        $info = $query->QueryOldPre17();
      }
    } catch (Throwable $e) {

    }

    if ($query !== null) {
      $query->Close();
		}


    // if (false AND !$isHost) $info['status'] = 'restarting';
    $info['status'] = (!isEmpty($info)) ? 'online' : 'offline';

		return $info;
  }

  /* ******************************** *
   *  Обработка данных
   *  Получение и отправка статистики
   *  в базу данных
   * ******************************** */

  public static function getStatus($data) {
    $roundUpDate   =  date("H:") . roundUp(date("i"), 2)   . date(" d.m.Y");
    $roundDownDate =  date("H:") . roundDown(date("i"), 2) . date(" d.m.Y");

    $dbData = @DB::fetchAll("SELECT * FROM " . DB_PREFIX . "minecraft_online WHERE `date` = '{$roundDownDate}' AND `name` = '{$data['name']}'")[0];

    if (isEmpty($dbData)) {
      $parseData = self::parseStatus($data, $isHost = false);
      $dbData = [
        'name' => @preg_replace('/(§\w)/iu', '', $data['name']) ?? null,
        'online' => $parseData['players']['online'] ?? 0,
        'slots' => $parseData['players']['max'] ?? 0,
        'status' => $parseData['status'],
      ];
      $query = DB::query("INSERT INTO `ml_minecraft_online` (`name`, `online`, `slots`, `status`, `date`) VALUES ('{$data['name']}', '{$dbData['online']}', '{$dbData['slots']}', '{$dbData['status']}', '{$roundUpDate}')");
    }

    return $dbData;
  }

}
