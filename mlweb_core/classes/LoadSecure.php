<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Аналог простого if(!defined) с красивым оформленем
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class LoadSecure {
  public $allow_access = false;

  function __construct() {
    if (!defined('_MLWEB')) {
      $geo = '';

      // IP
      $client  = @$_SERVER['HTTP_CLIENT_IP'];
      $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote  = @$_SERVER['REMOTE_ADDR'];

      if (filter_var($client, FILTER_VALIDATE_IP)) $ip = $client;
      elseif (filter_var($forward, FILTER_VALIDATE_IP)) $ip = $forward;
      else $ip = $remote;

      // Geolocation
      $row = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
      if ($row && $row->geoplugin_continentName != null) $geo .= $row->geoplugin_continentName;
      if ($row && $row->geoplugin_countryName != null) $geo .= '\\' . $row->geoplugin_countryName;
      if ($row && $row->geoplugin_region != null) $geo .= ', ' . $row->geoplugin_region;
      if ($row && $row->geoplugin_city != null) $geo .= ' ' . $row->geoplugin_city;

      // Message
      $message = "Привет!<br>
                  Тебе родители говорили, что так делать плохо?<br>
                  Ну так вот, так делать плохо!<br>
                  В общем либо ты прямо сейчас сваливаешь с данной страницы<br>
                  или мы отправляем твом данные Федеральной службе безопасности Российской Федерации<br><br>";

      $message .= "IP: $ip<br>";
      if ($geo) $message .= "Местопложение: $geo<br>";

      die($message);
    } else {
      $allow_access = true;
    }
  }
}