<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Класс обработки файлов конфигурации движка
* ***************************************************************** */

if (!defined('_MLWEB')) die('Доступ запрещен!');

class CFG {
  public static $config = [];

  private static $CONFIG_PATH = CONFIG_DIR . '/web';

  /* ******************************** *
    *  Подключение конфигурационных
    *  файлов
  * ********************************* */

  function __construct() {
    // Получаем все файлы из папки с конфигами
    foreach (scandirs(self::$CONFIG_PATH) as $file) {
      if (pathinfo($file, PATHINFO_EXTENSION) == 'php') {
        // Проверяем конфиг и парсим его
        $file_name = self::$CONFIG_PATH . '/' . $file;
        if(fileperms($file_name) != '0777') @chmod($file_name, 0777);         // Ставим права на запись, если их нет
        $file_data = include_once $file_name;
        foreach ($file_data as $name => $data) {
          if (@isJson($data)) $file_data[$name] = @json_decode($data, true);
        }
        // Добавляем конфиг в общий массив конфигураций
        self::$config = array_merge_recursive(self::$config, $file_data);
      }
    }
  }

  /* ******************************** *
    *  Получение настройки
  * ********************************* */

  public static function get($name) {
    return @self::$config[$name] ?? null;
  }

  /* ******************************** *
    *  Установка настроек
  * ********************************* */

  public static function set($data) {
    if (!is_array($data) OR isEmpty($data)) return false;

    $configFiles = [];

    // Ищем нужный нам файл конфигурации
    foreach (scandirs(CONFIG_DIR) as $folder_name => $files) {
      if (is_array($files)) {
        foreach ($files as $file) {
          $filePath = CONFIG_DIR . "/{$folder_name}/" . $file;
          $tmp = include $filePath;
          foreach ($data as $key => $value) {
            if (@!isEmpty($tmp[$key])) {
              $configFiles[$filePath][] = [$key => $value];
              // if ((count($configFiles) * 2) == count($data)) break 2;
            }
          }
        }
      } else {
        $filePath = CONFIG_DIR . '/' . $files;
        $tmp = include $filePath;
        foreach ($data as $key => $value) {
          if (@!isEmpty($tmp[$key])) {
            $configFiles[$filePath][] = [$key => $value];
            // if ((count($configFiles) * 2) == count($data)) break 2;
          }
        }
      }
    }

    // Изменяем настройки
    foreach ($configFiles as $filePath => $data) {
      // $filePath = CONFIG_DIR . $fileName;                   // Путь к файлу
      $file_orig = file($filePath);                         // Данные строк файла
      $file = $file_orig;
      $fileData = include $filePath;                        // Данные файла в виде массива
      foreach ($data as $data_) {
        foreach ($data_ as $key => $value) {
          $fileData[$key] = $value;                         // Изменяем настройки
        }
      }
      $fp = fopen($filePath, 'w');
      for ($i=0; $i <= sizeof($file_orig); $i++) {
        if ($i >= 9) {
          unset($file[$i]);                                 // Удаляем все строки после 9й стройки включительно
        }
      }
      $file[] = 'return ' . convertArrayToCode($fileData);  // Добавляем новый конфиг
      fputs($fp, implode('', $file));
      fclose($fp);
    }
  }


}
