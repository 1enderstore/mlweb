<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *     Класс для измерения времени
  *     выполнения скрипта или операций
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class TimerExecute {
  private static $start = .0;
  // private static $result = .0;

  static function start() {
    self::$start = microtime(true);
  }

  static function finish($countNumbers = 99) {
    return rtrim(number_format(microtime(true) - self::$start, $countNumbers, '.', ''), 0);
  }
}