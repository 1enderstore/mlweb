<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Криптография, шифрование, печеньки и котики
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class Security {
  private static $secret_key;

  /* ******************************** *
    *  Генерация secret_key
  * ********************************* */

  public static function generateSecretKey($saveKey = false) {
    $key = hash('sha256', randString(2048) . microtime());

    if ($saveKey) CFG::set(['secret_key' => $key]);

    return $saveKey ? true : $key;
  }

  /* ******************************** *
    *  AES DES Encrypt
  * ********************************* */

  public static function DESEncrypt($text, $secret_key = null) {
    if ($secret_key == null) $secret_key = CFG::get('secret_key');
    $encription_key = base64_decode($secret_key);
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-128-cbc'));
    $encripted = openssl_encrypt($text, 'aes-128-cbc', $encription_key, 0, $iv);
    $outText = base64_encode($encripted."::".$iv);
    return $outText;
  }

  /* ******************************** *
    *  AES DES Decrypt
  * ********************************* */

  public static function DESDecrypt($text, $secret_key = null) {
    if ($secret_key == null) $secret_key = CFG::get('secret_key');
    $encription_key = base64_decode($secret_key);
    list($encripted_data, $iv) = array_pad(explode('::', base64_decode($text),2), 2, null);
    $outText = openssl_decrypt($encripted_data, 'aes-128-cbc', $encription_key, 0, $iv);
    return $outText;
  }

}
