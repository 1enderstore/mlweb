<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *     Все для юзера
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class User {
  public static $data = [];

  function __construct() {
    if (isset($_SESSION['username'])) {
      // Уничтожение сессии по истечению времени жизни
      if ((time() - abs(@$_SESSION['time'])) >= @$_SESSION['session_delay'] * 3600) {
        self::unAuth();
      } else {
        self::$data = $_SESSION;
      }
    }
  }

  /* ******************************** *
    *  Шифрует пароль в его хэш
  * ********************************* */

  public static function encodePassword($password, $ver = 1) {
    if (isEmpty($password)) return false;

    switch ($ver) {
      case '1':
        return md5(hash('sha256', ($password)));
        break;

      case '2':
        return password_hash($password, PASSWORD_DEFAULT, ['salt' => randString(32)]);
        break;

      default:
        return false;
        break;
    }

    // return md5(hash('sha256', ($password)));
    // return password_hash($password, PASSWORD_DEFAULT, ['salt' => randString(32)]);
  }

  /* ******************************** *
    *  Проверка пароля
  * ********************************* */

  public static function verifyPassword($password, $passwordOrig, $salt = null) {
    if (isEmpty($password) or isEmpty($passwordOrig)) return false;
    // return self::encodePassword($password) == $row['password'];

    if (self::encodePassword($password, 1) == $passwordOrig) return true;
    else if (!isEmpty($salt) and password_verify($password, $salt)) return true;
    else return false;
  }

  /* ******************************** *
    *  Сжатая авторизация
  * ********************************* */

  public static function authShort($username, $password) {
    $row = DB::fetchAssoc("SELECT * FROM " . DB_PREFIX . "users WHERE `username` = '{$username}' OR `email` = '{$username}'");

    if (self::verifyPassword($password, $row['password'], @$row['salt'])) {
      $time = time();
      DB::fetchAll("UPDATE " . DB_PREFIX . "users SET `last_login` = '{$time}' WHERE `id` = '{$row['id']}'");

      if (isEmpty($row['uuid'])) {
        $row['uuid'] = self::getPlayerUUID($username);
        DB::fetchAll("UPDATE " . DB_PREFIX . "users SET `uuid` = '{$row['uuid']}' WHERE `id` = '{$row['id']}'");
      }

      self::$data['email']    = $row['email'];
      self::$data['id']       = $row['id'];
      self::$data['ip']       = getUserIP();
      self::$data['time']     = $time;
      self::$data['username'] = $row['username'];
      // self::$data['uuid']     = $row['uuid'];

      // @LOG::add("auth", @LNG::getText("auth_logged_in"), '', $username, "site");

      return true;
    }
    return false;
  }

  /* ******************************** *
    *  Полная авторизация
  * ********************************* */

  public static function auth($username, $password, $remember = false) {
    // $hash = md5($username . $password . $user_ip . "MiniLife <3");

    if (self::authShort($username, $password)) {
      self::$data['remember'] = $remember;

      $_SESSION = self::$data;

      if ($remember) $_SESSION['session_delay'] = 24 * 7;
      else $_SESSION['session_delay'] = 24;

      return true;
    } else return false;
  }

  /* ******************************** *
    *  Деавторизация
  * ********************************* */

  public static function unAuth() {
    session_destroy();
    self::$data = [];
  }

  /* ******************************** *
    *  Проверка авторизации
  * ********************************* */

  public static function isAuth() {
    return (
      isset($_SESSION['username']) &&
      isset($_SESSION['ip']) &&
      isset($_SESSION['session_delay'])
    ) ? true : false;
  }

  /* ******************************** *
    *  Проверка занятости ника
  * ********************************* */

  public static function checkUsername($username) {
    $row = DB::fetchAssoc("SELECT `id` FROM " . DB_PREFIX . "users WHERE `username` = '{$username}'");
    return (!isEmpty($row)) ? true : false;
  }

  /* ******************************** *
    *  Проверка занятости email
  * ********************************* */

  public static function checkEmail($email) {
    if (isEmpty($email))
      return false;
    $row = DB::fetchAssoc("SELECT `id` FROM " . DB_PREFIX . "users WHERE `email` = '{$email}'");
    return (!isEmpty($row)) ? true : false;
  }

  /* ******************************** *
    *  Валидатор ника
  * ********************************* */

  public static function validationUsername($username) {
    if (!preg_match('/^(?=.*[A-Za-z])[0-9A-Za-z\_]{4,16}$/', $username)) return true;
    else return false;
  }

  /* ******************************** *
    *  Валидатор пароля
  * ********************************* */

  public static function validationPassword($password) {
    if (preg_match('/^(?=.*[A-Za-z])[0-9A-Za-z!]{4,32}$/', $password)) return true;
    else return false;
  }

  /* ******************************** *
    *  Валидатор почты
  * ********************************* */

  public static function validationEmail($email) {
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) return false;
    if (!preg_match('/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{4,64})$/', $email)) return true;
  }

  /* ******************************** *
    *  Регистрация
    *
    *  1 - все ок
    *  2 - не тот ник
    *  3 - не тот email
  * ********************************* */

  public static function register($username, $email, $password, $hard = false) {
    if (self::checkUsername($username)) return 2;
    if (self::checkEmail($email)) return 3;

    $time     = time();
    $reg_ip   = getUserIP();
    $reg_date = $time;
    $password = self::encodePassword($password);
    $uuid     = getPlayerUUID($username);

    DB::exec("INSERT INTO " . DB_PREFIX . "users (username, email, password, reg_date, reg_ip, uuid) VALUES ('{$username}', '{$email}', '{$password}', '{$reg_date}', '{$reg_ip}', '{$uuid}')");
    return 1;
  }

  /* ******************************** *
    *  Изменение пароля
  * ********************************* */

  public static function changePassword($username, $oldpassword = null, $newpassword, $forcibly = false) {
    $row = DB::fetchAssoc("SELECT `id`, `username`, `email`, `password` FROM " . DB_PREFIX . "users WHERE `username` = '{$username}' OR `email` = '{$username}'");

    if (self::encodePassword($oldpassword) != $row['password'] && !$forcibly) return false; // TODO
    if (!self::validationPassword($newpassword) && !$forcibly) return false;                              // TODO

    $newpassword = self::encodePassword($newpassword);

    d("UPDATE " . DB_PREFIX . "users SET `password` = '{$newpassword}' WHERE `id` = '{$row['id']}'");
    DB::fetchAll("UPDATE " . DB_PREFIX . "users SET `password` = '{$newpassword}' WHERE `id` = '{$row['id']}'");

    return true;
  }

  /* ******************************** *
    *  Генерация ссылки
    *  на подтверждение регистрации
  * ********************************* */

  public static function getUserData($user = null) {
    if ($user == null AND isset($_SESSION['username'])) $user = $_SESSION['username'];

    $data = DB::fetchAssoc("SELECT * FROM " . DB_PREFIX . "users WHERE `username` = '{$user}' OR `email` = '{$user}' OR `id` = '{$user}'") ?? null;

    if (isEmpty($data)) $data['username'] = LNG::getText('not_found');

    $data = array_merge($data, [
      'avatar' => formatLink(checkImage(USERS_DIR . '/' . $data['username'] . '/avatar.png', 'avatar')),
      'skin'   => formatLink(checkImage(USERS_DIR . '/' . $data['username'] . '/skin.png', 'skin')),
      'clock'  => formatLink(checkImage(USERS_DIR . '/' . $data['username'] . '/clock.png', 'clock')),
    ]);

    return $data;
  }

  /* ******************************** *
    *  Генерация ссылки
    *  на подтверждение регистрации
  * ********************************* */

  public static function generateURLToConfirmRegister($username) {

  }

  /* ******************************** *
    *  Генерация кода подтверждения
  * ********************************* */

  public static function generateConfirmCode($username_id) {
    $row = self::getUserData($username_id);
    $params = [
      $row['id'],
      $row['username'],
      $row['email'],
      getUserIP(),
      time(),
    ];
    return Security::DESEncrypt(implode(';', $params));
  }

  /* ******************************** *
    *  Расшифровка подтвеждающего кода
  * ********************************* */

  public static function decodeConfirmCode($code) {
    $tmp = explode(';', Security::DESDecrypt($code));
    return [
      'id'       => $tmp[0],
      'username' => $tmp[1],
      'email'    => $tmp[2],
      'ip'       => $tmp[3],
      'time'     => $tmp[4],
    ];
  }

  /* ******************************** *
   *  Генерирование UUID
   * ********************************* */

  public static function getPlayerUUID($username) {
    $data = hex2bin(md5($username));
    $data[6] = chr(ord($data[6]) & 0x0f | 0x30);
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    return self::createJavaUUID(bin2hex($data));
  }

  public static function createJavaUUID($striped) {
    $components = array(
      substr($striped, 0, 8),
      substr($striped, 8, 4),
      substr($striped, 12, 4),
      substr($striped, 16, 4),
      substr($striped, 20),
    );
    return implode('-', $components);
  }

}
