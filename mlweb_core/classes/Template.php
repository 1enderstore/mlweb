<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Отвечает за темы и дизайн
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class Template {
  public static  $template_default   = ['main'];
  public static  $template           = ['main'];
  public static  $tpl                = '';
  public static  $content            = '';
  public static  $notices            = [];
  public static  $title              = [0 => '', 1 => false];
  public static  $title_original     = '';
  public static  $description        = '';
  public static  $keywords           = '';
  public static  $underfined_title   = 'Title not set';
  public static  $vars               = [];
  private static $n_register         = 0;

  /* ******************************** *
    *  Загрузить файл шаблона
  * ********************************* */

  public static function load($template) {
    if ($template == 'main') return false;
    self::$template[] = $template;
  }

  /* ******************************** *
    *  Загрузить файл шаблона
    *  по умолчанию
  * ********************************* */

  public static function defaultLoad($template, $hard = false) {
    if ($hard) self::$template = [];
    self::$template[0] = $template;
  }

  /* ******************************** *
    *  Загрузить файл шаблона
    *  по умолчанию
  * ********************************* */

  public static function loadTemplate($template) {
    switch ($template) {
      case 'home':
        self::$template = self::$template_default;
        self::setTitle(CFG::get('title'));
        header('Location: /');
        break;
    }
  }

  /* ******************************** *
    *
  * ********************************* */

  public static function error($type, $code) {
    self::defaultLoad('errors/' . $code, true);
    self::setTitle(LNG::getText('error_' . $code));
  }

  /* ******************************** *
    *  Регистрирование
    *  переменных
  * ********************************* */

  public static function registerVar($obj, $var) {
    if (is_array($var)) return false;  // Может быть в будущем резрешим вывод массивов, но смысл ¯\_(ツ)_/¯
    // $obj = self::formatVar($obj);
    self::$vars[] = ['obj' => $obj, 'var' => $var];
  }

  /* ******************************** *
    *  Устанвоить переменную
  * ********************************* */

  public static function setVar($obj, $var, $template = '') {
    if (is_array($var)) return false;
    $obj = self::formatVar($obj);
    return str_replace($obj, $var, $template);
  }
  /* ******************************** *
    *  Регистрирование
    *  переменных
  * ********************************* */

  public static function registerNotice($type, $title, $message) {
    self::$notices[] = [
      'type' => $type,
      'title' => $title,
      'message' => $message,
    ];
  }

  /* ******************************** *
    *  Проверка наличия переменной
    *  в шаблонах
  * ********************************* */

  public static function checkVar($var) {

  }

  /* ******************************** *
    *  Проверка наличия переменной
    *  в шаблонах
  * ********************************* */

  public static function formatVar($var) {
    return '{{$'.$var.'}}';
  }

  /* ******************************** *
    *  Получить переменную
  * ********************************* */

  public static function getVar($var) {
    // d(self::$vars, 1);
    return $var;
  }

  /* ******************************** *
    *  Регистрирование
    *  заголовка страницы
  * ********************************* */

  public static function setTitle($title, $hard = false) {
    if (self::$title[0] !== $title) {
      self::$title[0] = $title;
      if (self::$title[1] != $hard) {
        self::$title[1] = $hard;
      }
    } else {
      if (self::$title[1] != $hard) {
        self::$title[1] = $hard;
      }
    }
  }

  /* ******************************** *
    *  Регистрирование
    *  описания страницы
  * ********************************* */

  public static function setDescription($description) {
    if (self::$description !== $description) self::$description = $description;
  }

  /* ******************************** *
    *  Вставка кода
  * ********************************* */

  public static function pasteCode($tag, $code, $position = "before / after") {
    if ($position == "before / after") return false;

    if ($position == "before") $code = $code . PHP_EOL . $tag;
    else $code = $tag . PHP_EOL . $code;

    self::registerVar($tag, $code, false);
  }

  /* ******************************** *
    *
  * ********************************* */

  public static function setContent($content) {
    self::$content = $content;
  }

  /* ******************************** *
    *  Генерация HEADER страницы
  * ********************************* */

  public static function generateHeader() {
    $lang = LNG::getLang();

    $db_data_ = DB::fetchAll("SELECT * FROM " . DB_PREFIX . "pages WHERE `name` = '" . URLHandler::getType() . "'");
    $db_data = [];

    foreach ($db_data_ as $key => $value) {
      $db_data[$value['lang']] = $value;
    }

    // TITLE
    if (self::$title[1] == true)
      $title = self::$title[0];
    elseif (isset($db_data[$lang]['lang']) AND !isEmpty($db_data[$lang]['title']))
      $title = $db_data[$lang]['title'];
    elseif (isset($db_data['en_US']['lang']) AND !isEmpty($db_data['en_US']['title']))
      $title = $db_data['en_US']['title'];
    else
      $title = self::$title[0];

    if (isEmpty($title)) {
      if (!isEmpty(CFG::get('title'))) $title = CFG::get('title');
      else $title = self::$underfined_title;
    }

    // Префикс / суффикс заголовка страницы
    if (CFG::get('title_mode') == 1)
      $title = CFG::get('title_mode') . CFG::get('title_add') . $title;
    else if (CFG::get('title_mode') == 2)
      $title = $title . CFG::get('title_add') . (CFG::get('title__' . $lang) ?? CFG::get('title__en_US'));

    // Установка заголовка
    self::$title[0]       = $title;
    self::$title_original = $title;


    // DESCRIPTION
    if (isset($db_data[$lang]['lang']) AND !isEmpty($db_data[$lang]['description'])) {
      $description = $db_data[$lang]['description'];
    } elseif (isset($db_data['en_US']['lang']) AND !isEmpty($db_data['en_US']['description'])) {
      $description = $db_data['en_US']['description'];
    } else {
      $description = self::$description;
    }

    if (isEmpty($description)) $description = CFG::get('description');

    self::$description = $description;


    // KERWORDS
    if (isset($db_data[$lang]['lang']) AND !isEmpty($db_data[$lang]['keywords'])) {
      $keywords = $db_data[$lang]['keywords'];
    } elseif (isset($db_data['en_US']['lang']) AND !isEmpty($db_data['en_US']['keywords'])) {
      $keywords = $db_data['en_US']['keywords'];
    } else {
      $keywords = self::$keywords;
    }

    if (isEmpty($keywords)) $keywords = CFG::get('keywords');

    self::$keywords = $keywords;

  }

  /* ******************************** *
    *  Генерация шаблона из
    *  исходников
  * ********************************* */

  public static function generate() {
    if (!file_exists(TEMPLATE_DIR) && is_file(TEMPLATE_DIR)) return false;

    foreach (self::$template as $key => $value) {
      $path = TEMPLATE_DIR . "/{$value}.php";

      if (file_exists($path) && is_file($path)) {
        if ($key == 0)
          self::$tpl .= file_get_contents($path);
        else
          self::$content .= file_get_contents($path);
      } else {
        $error_text = LNG::getText('notfound_template_file', [CFG::_('theme'), $value]);
      }
    }

    /* ******************************** *
      *  Прелоэдер
    * ********************************* */

    if (CFG::get('enable_preloader') == true) {
      $preloaderTemplate = file_get_contents(TEMPLATE_DIR . '/preloader.php');
      self::$content = $preloaderTemplate . self::$content;
    }

    /* ******************************** *
      *  Обработка уведомлений и контента
    * ********************************* */

    $notices = '';
    $noticeTemplate = file_get_contents(TEMPLATE_DIR . '/notice.php');

    foreach (self::$notices as $notice) {
      $tmp = $noticeTemplate;
      $tmp = Template::setVar('type', $notice['type'], $tmp);
      $tmp = Template::setVar('title', $notice['title'], $tmp);
      $tmp = Template::setVar('message', $notice['message'], $tmp);
      $notices .= $tmp;
    }

    self::$content = $notices . self::$content;

    self::$tpl = str_replace(self::formatVar("content"), self::$content, self::$tpl);

    /* ******************************** *
      *  Назначение зарегистрированных
      *  переменных
    * ********************************* */

    foreach (self::$vars as $var) {
      self::$tpl = str_replace(self::formatVar($var['obj']), $var['var'], self::$tpl);
    }

    /* ******************************** *
      *  Удаление мусора
    * ********************************* */

    self::$tpl = preg_replace("/\{\{(.*?){0,32}\}\}/", '', self::$tpl);
  }

  /* ******************************** *
    *  Регистрирование
    *  описания страницы
  * ********************************* */

  public static function getBreadcrumbs() {
    $breadcrumbs = '';
    $breadcrumbs .= "<li><a href=\"/\"><i class=\"fas fa-home\"></i>Главная</a></li>";

    return $breadcrumbs;
  }

}
