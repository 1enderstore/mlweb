<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Основные функции для API
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class API {

  /* ******************************** *
    *  Конвертация ответа в JSON
  * ********************************* */

  public static function responseConvert($data, $code = null) {
    $responce = array_merge([], $data);
    if (strlen($code) > 0) $responce['code'] = $code;
    return json_encode($responce, JSON_UNESCAPED_UNICODE);
  }

  /* ******************************** *
    *  Вывод ответа
  * ********************************* */

  public static function response($data, $code = null) {
    // echo("<pre style=\"word-wrap: break-word; white-space: pre-wrap;\">" . self::responseConvert($data, $code) . "</pre>");
    return self::responseConvert($data, $code);
  }

  /* ******************************** *
    *  Получение метода
  * ********************************* */

  public static function getMethod() {
    return self::valideMethod(URLHandler::$data['url']['method']);
  }

  /* ******************************** *
    *  Проверка метода
    *
    *  @Получает на вход:
    *    Данные с $data['url']['method']
    *  @На выходе:
    *    Если находит метод в папке ~/api/methods, то выводит его
    *    Если не находит, то false
  * ********************************* */

  public static function valideMethod($method) {
    if (
      preg_match("/^[aA-zZ0-9\.]+$/", $method) &&
      ((substr_count($method, '.') <= 1)) && (substr_count($method, '.') >= 0) &&
      is_file(METHODS_DIR . '/' . $method . '.php')
    ) return $method;
    else return false;
  }

}
