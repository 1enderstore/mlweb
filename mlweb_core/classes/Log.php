<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Слежение за действиями пользователей
  *  Автор:
  *    GumanXD
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

class LOG {

  /* ******************************** *
    *  запись логов
  * ********************************* */

  public static function add($file, $status, $dir, $username = "", $stie = "") {
    $files = [
      "system"      => "main.log",
      "error"       => "error.log",
      "auth"        => "auth.log",
      "transaction" => "transaction.log",
    ];

    $date = date("d.m.Y H:i:s");

    if ($file == "error") {
      $log  = fopen(LOGS_DIR . "/" . $files[$file], "a+t");
      $text = "{$date} [{$file}]: {$status}, From: {$dir}\n";

    } elseif ($file == "system") {
      $log  = fopen(LOGS_DIR . "/" . $files[$file], "a+t");
      $text = "{$date} [{$file}]: {$status}, From: {$dir}\n";

    } elseif ($file == "auth") {
      if (!isEmpty($username)) return false;
      if (!is_dir(LOGS_DIR . "/" . $username) && !file_exists(LOGS_DIR . "/" . $username)) {
        @mkdir(LOGS_DIR . "/" . $username, 0700);
      }
      $sites = [
        "site"        => "сайте",
        "forum"       => "форуме",
        "admin"       => "админ панеле",
        "transaction" => "transaction.log",
      ];

      $log  = fopen(LOGS_DIR . "/" . $username . "/" . $files[$file], "a+t");
      $text = "{$date} [{$username}]: {$status} на {$sites[$site]}\n";

    }

    fwrite($log, $text);
    fclose($log);
  }

}