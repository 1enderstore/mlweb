<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Ядро движка
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

/* ******************************** *
  *  Автоподключение модулей
* ********************************* */

if (CFG::get('offline_mode') == true && URLHandler::$data['system'] == false) {
  include_once HANDLERS_DIR . '/offline_mode.php';                                            // Страница оффлайн режима

} elseif (URLHandler::$data == '[404]') {
  Template::error('web', '404');                                                              // Обработка ошибок

} elseif (isset(URLHandler::$data['handler']) && !isEmpty(URLHandler::$data['handler'])) {
  include_once URLHandler::$data['handler'];                                                  // Подключение обработчика
  if (isset(URLHandler::$data['hard']) && URLHandler::$data['hard'] == true)
    exit;                                                                                     // Остановка дальнейшего выполнения при hard == true

} elseif (URLHandler::getType() !== 'home') {
  Template::error('system', 'handler-not-found');                                             // Если нет обработчика, то выводим ошибку

}

/* ************************************ *
  *  DEBUGER
* ************************************ */

@Debug::add('LastUpdate', date('H:i:s d.m.y'));
@Debug::add('$_SESSION', $_SESSION);
// @Debug::add('$_SERVER', $_SERVER);

// User::changePassword('1EnderStore', '', '1EnderStore', 1);

/* ************************************ *
  *  Загрузка и генерация шаблона
* ************************************ */

include_once HANDLERS_DIR . '/MainPage.php';

/* ************************************ *
  *  DEBUGER
* ************************************ */

@Debug::write(ROOT_DIR . '/debug.txt');
