<?php
/* **************************************************************** *
 *  MiniLife - Your little life...
 *  Назначение:
 *    Парсинг данных серверов Minecraft
 * ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

/* ******************************** *
 *  Основной код
 * ********************************* */

// include_once CLASSES_DIR . '/server-parser.php';
use xPaw\MinecraftPing;
use xPaw\MinecraftPingException;

include LIBS_DIR . '/PHP-Minecraft-Query/MinecraftPing.php';
include LIBS_DIR . '/PHP-Minecraft-Query/MinecraftPingException.php';

$Timer = MicroTime(true);

$Info = false;
$Query = null;

try
{
  $Query = new MinecraftPing('s8.mcskill.ru', 25510);
  // $Query = new MinecraftPing('s2.mcskill.ru', 25999);

  $Info = $Query->Query();

  if ($Info === false) {
    /*
     * If this server is older than 1.7, we can try querying it again using older protocol
     * This function returns data in a different format, you will have to manually map
     * things yourself if you want to match 1.7's output
     *
     * If you know for sure that this server is using an older version,
     * you then can directly call QueryOldPre17 and avoid Query() and then reconnection part
     */

    $Query->Close();
    $Query->Connect();

    // $Info = $Query->QueryOldPre17( );
  }
} catch (Throwable $e) {

}

if ($Query !== null) {
  $Query->Close();
}

// $q = new MinecraftQuery('s9.mcskill.ru', 25475);
$response = $Info;
