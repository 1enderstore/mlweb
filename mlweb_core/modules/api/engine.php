<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Ядро API
* ***************************************************************** */

if (URLHandler::getType() !== 'api' || !defined('_MLWEB')) die("Доступ запрещен!");

/* ******************************** *
  *  Старт
* ********************************* */

$method = API::getMethod();
if (empty($method) && !$method) API::response(LNG::getText("api__method_not_found"), 100, true);

/* ******************************** *
  *  Защита от спама
* ********************************* */

// // Генерация отпечатка пользователя
// $fingerprint = hash('sha256', ($_SERVER["HTTP_USER_AGENT"] . $_SERVER['HTTP_ACCEPT_LANGUAGE'] . gethostbyaddr(getUserIP())));
// // Получение данных
// $row = DB::fetchAssoc("SELECT * FROM " . DB_PREFIX . "ajax_cache WHERE `fingerprint` = '{$fingerprint}'");

// if (!isEmpty($row)) {
//   $row['data'] = json_decode($row['data'], true);

//   if (isset($row['data'])) $data = (array)$row['data'];
//   else $data = [];
//   if (isset($row['data'][0][$method]['last_query'])) $last_query = $row['data'][0][$method]['last_query'];
//   else $last_query = 0;
//   if (isset($row['data'][0][$method]['query_count'])) $query_count = $row['data'][0][$method]['query_count'];
//   else $query_count = 0;

//   $die = false;
//   $code = null;

//   if (abs(rtrim(number_format(microtime(true) - $last_query, 2, ".", ""), 0)) <= 0.5) {
//     $die = true;
//   } else $query_count = 0;

//   $data[0][$method]['last_query'] = microtime(true);
//   $query_count++;
//   $data[0][$method]['query_count'] = $query_count;

//   $data = json_encode($data);
//   DB::exec("UPDATE " . DB_PREFIX . "ajax_cache SET `data` = '{$data}' WHERE `fingerprint` = '{$fingerprint}'");
//   if ($die) API::response(LNG::getText('api__spamer'), 101, true);
// } else {
//   $data[0][$method]['last_query'] = microtime(true);
//   $data[0][$method]['query_count'] = 1;
//   $data = json_encode($data);
//   DB::exec("INSERT INTO " . DB_PREFIX . "ajax_cache (fingerprint, data) VALUES ('{$fingerprint}', '{$data}')");
// }

/* ******************************** *
  *  Обработка методов
* ********************************* */

include_once(METHODS_DIR . '/' . $method . '.php');
