<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Авторизация пользователя
* ***************************************************************** */

if(URLHandler::getType() !== 'api' || !defined('_MLWEB')) die("Доступ запрещен!");

/* ******************************** *
  *  Проверка на наличие
  *  переменных
* ********************************* */

$data = [];

// Username
if (isset($_GET['username']) && !isEmpty($_GET['username'])) $data['username'] = $_GET['username'];
else API::response(LNG::getText('api__empty_fields'), 102, true);

// Password
if (isset($_GET['password']) && !isEmpty($_GET['password'])) $data['password'] = $_GET['password'];
else API::response(LNG::getText('api__empty_fields'), 102, true);

// Language
if (isset($_GET['lang']) && !isEmpty($_GET['lang'])) $data['lang'] = $_GET['lang'];
else $data['lang'] = '';

// IP
if (isset($_GET['ip']) && !isEmpty($_GET['ip'])) $data['ip'] = $_GET['ip'];
else $data['ip'] = '';

// Full auth
// if (isset($_GET['full']) && !isEmpty($_GET['full'])) $data['full'] = $_GET['full'];
// if (!isset($data['full'])) $data['full'] = false;

// Type auth
if (isset($_GET['auth_type']) && !isEmpty($_GET['auth_type'])) $data['auth_type'] = $_GET['auth_type'];
if (!isset($data['auth_type'])) $data['auth_type'] = false;

// Remember
if (isset($_GET['remember']) && !isEmpty($_GET['remember'])) $data['remember'] = $_GET['remember'];
if (isset($data['remember']) && ($data['remember'] == 'yes')) $data['remember'] = true;
else $data['remember'] = false;

/* ******************************** *
  *  Основной код
* ********************************* */

$code = null;
$response = [];

// if ($data['auth_type'] == 'launcher') {
//   if (User::authShort($data['username'], $data['password'])) {
//     $response['username'] = $data['username'];
//     $response['permissions'] = 0;  // TODO: добавить обработку и получение прав юзера
//   } else {
//     // $response['username']
//   }
// } else if (!User::isAuth($data['username']) /* Проверка на авторизацию */) {

//   switch ($data['auth_type']) {
//     // Полная или короткая авторизация
//     case 'full':
//       $response['text'] = User::auth($data['username'], $data['password'], $data['remember']);
//       $response['message'] = User::auth($data['username'], $data['password'], $data['remember']);
//       break;

//     // Авторизация в лаунчере
//     case 'launcher':
//       if (User::authShort($data['username'], $data['password'])) {
//         $response['username'] = $data['username'];
//         $response['permissions'] = 0;  // TODO: добавить обработку и получение прав юзера
//       }
//       break;

//     default:
//       $response['text'] = User::authShort($data['username'], $data['password'], $data['remember']);
//       $response['message'] = User::authShort($data['username'], $data['password'], $data['remember']);
//       break;
//   }

//   // if ($response) {
//   //   $response['text'] = LNG::getText('auth__success', [], $data['lang']);  // Successful authorization
//   //   $response['message'] = LNG::getText('auth__success', [], $data['lang']);  // Successful authorization
//   //   $code = 1;
//   // } else {
//   //   $response['text'] = LNG::getText('auth__invalid', [], $data['lang']);  // Invalid username or password
//   //   $response['message'] = LNG::getText('auth__invalid', [], $data['lang']);  // Invalid username or password
//   //   $code = 105;
//   // }
// } else {
//   $response['text'] = LNG::getText('auth__islogin', [], $data['lang']);  // You are already logged in
//   $response['message'] = LNG::getText('auth__islogin', [], $data['lang']);  // You are already logged in
//   $code = 103;
// }


switch ($data['auth_type']) {
  // Полная или короткая авторизация
  case 'full':
    $auth = User::auth($data['username'], $data['password'], $data['remember']);
    if ($auth == true) {
      $response['message'] = LNG::getText('auth__success', [], $data['lang']);
      $code = 1;
    } else {
      $response['message'] = LNG::getText('auth__invalid', [], $data['lang']);
      $code = 105;
    }
    marker();
    break;

  // Авторизация в лаунчере
  case 'launcher':
    if (User::authShort($data['username'], $data['password'])) {
      $response['username'] = $data['username'];
      $response['permissions'] = 0;  // TODO: добавить обработку и получение прав юзера
    }
    break;

  default:
    $auth = User::authShort($data['username'], $data['password'], $data['remember']);
    if ($auth == true) {
      $response['message'] = LNG::getText('auth__success', [], $data['lang']);
      $code = 1;
    } else {
      $response['message'] = LNG::getText('auth__invalid', [], $data['lang']);
      $code = 105;
    }
    break;
}

API::response($response, $code);
