<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Получение онлайна сервера Minecraft
* ***************************************************************** */

if(URLHandler::getType() !== 'api' || !defined('_MLWEB')) die("Доступ запрещен!");

include CLASSES_DIR . '/MinecraftServerPinger.php';

/* ******************************** *
  *  Проверка на наличие
  *  переменных
* ********************************* */

$data = [];

// Server
if (isset($_GET['server']) && !isEmpty($_GET['server'])) $data['server'] = $_GET['server'];
else $data['server'] = '';

// Host
if (isset($_GET['host']) && !isEmpty($_GET['host'])) $data['host'] = $_GET['host'];
else $data['host'] = '';

/* ******************************** *
  *  Основной код
* ********************************* */

$response = [];
$code = null;

// Обработка простых запросов
if (!empty($data['host'])) {
  getServerData($_GET['host']);
} elseif (!empty($data['server'])) {

  // Вывести все серверы
  if ($data['server'] == 'all') {
    $servers = DB::getData('minecraft_servers');
    for ($i=0; $i < count($servers); $i++) {
      if ($servers[$i]['enable'] == true) getServerData($servers[$i]);
    }
  }
  // Вывести один сервер
  else {
    $server = DB::getData('minecraft_servers', "`name` = '{$data['server']}'");
    if (isEmpty($server)) API::response(LNG::getText('api__server_not_found'), 0, true);
    $server['name'] = $data['server'];
    if ($server['enable'] == true) getServerData($server);
  }
}

function getServerData($data, $isHost = false) {
  global $response;

  $serverData = MinecraftServerPinger::getStatus($data);

  $response[] = [
    'name' => $serverData['name'],
    'online' => $serverData['online'],
    'slots' => $serverData['slots'],
    'status' => $serverData['status'],
  ];
}

$response = API::response($response, $code);
print_r($response);
