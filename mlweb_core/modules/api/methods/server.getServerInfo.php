<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Обработчик запуска сервера
* ***************************************************************** */

if(URLHandler::getType() !== 'api' || !defined('_MLWEB')) die("Доступ запрещен!");

/* ******************************** *
  *  Проверка на наличие
  *  переменных
* ********************************* */

$data = [];

// Server
if (isset($_GET['server']) && !isEmpty($_GET['server'])) $data['server'] = $_GET['server'];
else $data['server'] = '';

// IP
if (isset($_GET['ip']) && !isEmpty($_GET['ip'])) $data['ip'] = $_GET['ip'];
else $data['ip'] = '';

// Secret word
if (isset($_GET['secret_word']) && !isEmpty($_GET['secret_word'])) $data['secret_word'] = $_GET['secret_word'];
else $data['secret_word'] = '';

/* ******************************** *
  *  Основной код
* ********************************* */

$response = [];
$code = null;

$response['allowStart'] = true;
$response['serverTime'] = time();

$response = API::response($response, $code);
$response = Security::DESEncrypt($response);
print_r($response);


$response = Security::DESDecrypt($response);
print_r($response);