<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Деавторизация пользователя
* ***************************************************************** */

if(URLHandler::getType() !== 'api' || !defined('_MLWEB')) die("Доступ запрещен!");

/* ******************************** *
  *  Проверка на наличие
  *  переменных
* ********************************* */

$data = [];

// Username
if (isset($_GET['username']) && !isEmpty($_GET['username'])) $data['username'] = $_GET['username'];
else API::response(LNG::getText('api__empty_fields'), 102, true);

// Language
if (isset($_GET['lang']) && !isEmpty($_GET['lang'])) $data['lang'] = $_GET['lang'];
else $data['lang'] = '';

/* ******************************** *
  *  Основной код
* ********************************* */

$code = null;
$response = [];

if (@$_SESSION['ip'] == getUserIP()) {
  User::unauth($data['username']);
  $response['message'] = LNG::getText('code_1', [], $data['lang']);
  $code = 1;
} else {
  $response['message'] = LNG::getText('api__alien_nickname', [], $data['lang']) . ' ' . $data['username'];
  $code = 104;
}

API::response($response, $code);
