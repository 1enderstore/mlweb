<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Обработчик страницы оффлайн-режима
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

/* ******************************** *
  *  Основной код
* ********************************* */

Template::defaultLoad('offline_mode', true);
Template::setTitle(CFG::get('offline_pageTitle'), true);
Template::registerVar('offline_title', CFG::get('offline_title'));
Template::registerVar('offline_message', CFG::get('offline_message'));
