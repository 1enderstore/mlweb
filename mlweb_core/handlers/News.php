<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Обработчик новостей
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

/* ******************************** *
  *  Основной код
* ********************************* */

if (isset(URLHandler::$params['page'])) {
  $title_alt = URLHandler::$params['page'];
  $news_type = 'full';
}

if ($news_type == 'short') {
  $news = '';
  $template = file_get_contents(TEMPLATE_DIR . '/news_short.php');
  $query = DB::fetchAll("SELECT * FROM " . DB_PREFIX . "news WHERE 1 ORDER BY date DESC LIMIT 10");

  $i = 0;
  $news_count = sizeof($query);

  foreach ($query as $data) {
    $data['short_content'] = formatHTML($data['short_content']);
    $data['short_content'] = shorten($data['short_content'], 260);

    $tmp = $template;

    /* **************** *
      *  AUTHOR
    * ***************** */

    $userData = User::getUserData($data['author']);

    $tmp = Template::setVar('author', $userData['username'], $tmp);

    $tmp = Template::setVar('author_avatar', @$userData['avatar'], $tmp);

    /* **************** *
      *  POST
    * ***************** */

    $tmp = Template::setVar('id', $data['id'], $tmp);
    $tmp = Template::setVar('title', $data['title'], $tmp);
    $tmp = Template::setVar('content', $data['short_content'], $tmp);

    $image_url = UPLOADS_DIR . "/posts/{$data['id']}/preview.png";
    if (!checkImage($image_url)) $image_url = ASSETS_DIR . "/images/no-image.jpg";

    $tmp = Template::setVar('image_url', formatLink($image_url), $tmp);
    // $tmp = Template::setVar('image_url', $data['image_url'], $tmp);
    $tmp = Template::setVar('views_count', $data['views'], $tmp);
    $tmp = Template::setVar('likes_count', $data['likes'], $tmp);
    $tmp = Template::setVar('comments_count', $data['comments'], $tmp);
    $tmp = Template::setVar('date', formatDate($data['date'], false), $tmp);
    $tmp = Template::setVar('url_full', '/news/' . $data['title_alt'], $tmp);
    $news .= $tmp;

    $i++;
  }

  Template::setContent($news);

} elseif ($news_type == 'full') {
  if (!isEmpty($title_alt)) {
    $template = file_get_contents(TEMPLATE_DIR . '/news_full.php');
    $query = DB::fetchAll("SELECT * FROM " . DB_PREFIX . "news WHERE title_alt = '{$title_alt}'")[0];

    Template::setTitle($query['title']);

    $template = Template::setVar('id', $query['id'], $template);
    $template = Template::setVar('title', $query['title'], $template);
    $template = Template::setVar('content', $query['content'], $template);

    $image_url = UPLOADS_DIR . "/posts/{$query['id']}/preview.png";
    $template = Template::setVar('image_url', $image_url, $template);
    // $template = Template::setVar('image_url', $query['image_url'], $template);
    $template = Template::setVar('views_count', $query['views'], $template);
    $template = Template::setVar('likes_count', $query['likes'], $template);
    $template = Template::setVar('comments_count', $query['comments'], $template);
    $template = Template::setVar('date', $query['date'], $template);

    Template::setContent($template);

  } else {
    // TODO: Добавить обработку 404
  }
}