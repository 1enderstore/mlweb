<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Главная страница сайта
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

if (isEmpty(Template::$title) && URLHandler::getType() == 'home') {
  Template::loadTemplate('home');
}

if (URLHandler::getType() == 'home') {
  $news_type = 'short';
  include HANDLERS_DIR . '/News.php';
}

/* ******************************** *
  *  Регистрирование
  *  переменных
* ********************************* */

Template::pasteCode('</body>',
  PHP_EOL . '<!--'
. PHP_EOL . '___________$$$$$$ '
. PHP_EOL . '___________$$_____$$ '
. PHP_EOL . '__________$__(•)____$$ '
. PHP_EOL . '________$$__________$ '
. PHP_EOL . '___________$$_____$ '
. PHP_EOL . '___________$____$ '
. PHP_EOL . '_________$____$__$$$__$$______$ '
. PHP_EOL . '_______$$_____$_____$$__$$__$$$ '
. PHP_EOL . '_______$______$___________$$__$ '
. PHP_EOL . '_______$$_______$______$$_____$ '
. PHP_EOL . '_______$$________$$$$$$______$ '
. PHP_EOL . '________$$$________________$ '
. PHP_EOL . '__________$$$$__________$$ '
. PHP_EOL . '____________$$$$$$$$$$$$ '
. PHP_EOL . '?“????????“?¤ '
. PHP_EOL . '……......¤?“????????“?¤Утя¤?“????????“?¤...... '
. PHP_EOL . '-->' . PHP_EOL
, 'before');

/* ******************************** *
  *  Генерация шаблона
* ********************************* */

Template::generateHeader();
Template::generate();

/* ******************************** *
  *  Функции для вывода данных
* ********************************* */

function ml_title($no_prefix = false) {
  if (!$no_prefix) return Template::$title[0];
  else return Template::$title_original;
}

function ml_description() {
  return Template::$description;
}

function ml_keywords() {
  return Template::$keywords;
}

function ml_lang($full = false) {
  if ($full) return LNG::getLang();
  else return substr(LNG::getLang(), 0, 2);
}

function ml_time($format = false) {
  if ($format) return date('H:i:s d.m.y');  // https://www.php.net/manual/ru/function.date.php#example-2811
  else return time();
}

function ml_JSData() {
  $keys = [
    'username',
    'email',
    'lang',
  ];
  return json_encode(ml_get($keys), JSON_UNESCAPED_UNICODE);
}

function ml_get($vars) {
  $array = [
    'username' => @$_SESSION['username'] ? @$_SESSION['username'] : '',
    'email' => @$_SESSION['email'] ? @$_SESSION['email'] : '',
    'lang' => @LNG::getTreeLang(),
  ];
  $data;
  if (is_array($vars)) {
    // TODO: Тут кажись нет ключа массива с названием переменной, которую мы запрашиваем. Пример: ml_get(['email', 'lang'])
    foreach ($vars as $var) {
      $data[] = $array[$var];
    }
  } else {
    $data = $array[$vars];
  }

  return $data ?? '';
}

/* ******************************** *
  *  Вывод
* ********************************* */

eval(' ?' . '>' . Template::$tpl . '<' . '?php ');  // Отображение вывода генерации
