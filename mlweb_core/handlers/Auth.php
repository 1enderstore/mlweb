<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Обработчик страницы авторизации
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

/* ******************************** *
  *  Основной код
* ********************************* */

switch (str_replace('/', '', URLHandler::$url)) {
  case 'login':
    if (User::isAuth()) {
      Template::loadTemplate('home');
    } else {
      Template::load('login');
      Template::setTitle(LNG::getText('auth__title_login'));
    }
    break;

  case 'lostpass':
    if (User::isAuth()) {
      Template::loadTemplate('home');
    } else {
      Template::load('lostpass');
      Template::setTitle(LNG::getText('auth__title_lostpassword'));
    }
    break;
}
