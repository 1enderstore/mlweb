<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Обработчик страницы регистрации
* ***************************************************************** */

defined('_MLWEB') or die("Доступ запрещен!");

/* ******************************** *
  *  Основной код
* ********************************* */

if (User::isAuth()) {
  Template::loadTemplate('home');
} else {
  Template::load('register');
  Template::setTitle(LNG::getText('auth__title_register'));
}
