<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение: основные функции
* ***************************************************************** */

/* ******************************** *
  *  Функция дебага
  *  переменной
* ********************************* */

/**
 * Вывод переменной без декора
 * @param [String / Array] $[str]
 */
function d($str, $deep = false) {
  // if(empty($str)) return false;
  echo ("<pre>");
  if ($deep == true) var_dump($str);
  else print_r($str);
  echo ("</pre>");
}

/**
 * Вывод переменной без декора и с заменой html символов
 * @param [String] $[str]
 */
function dh($str, $deep = false) {
  // if(empty($str)) return false;
  $str = htmlspecialchars($str);

  echo ("<pre>");
  if ($deep == true) var_dump($str);
  else print_r($str);
  echo ("</pre>");
}

/* ******************************** *
  *  Функция подключения
  *  модулей
* ********************************* */

// function include_module($type, $file, $lang = null) {
//   $types = [
//     "ajax" => "/ajax/",
//     "class" => "/classes/",
//     "handler" => "/handlers/",
//     "lang" => "/languages/",
//     "lib" => "/libs/",
//     "module" => "/modules/",
//   ];

//   if ($type == 'lang') $path = CORE_DIR . $types[$type] . $lang . '/' . $file . '.json';
//   else $path = CORE_DIR . $types[$type] . $file . '.php';

//   if (is_file($path)) {
//     if ($type == 'lang') return json_decode(file_get_contents($path), true);
//     else include($path);
//   } else return false;
// }

/* ******************************** *
  *  Проверка на пустоту
* ********************************* */

function isEmpty($var) {
  if     (empty($var))  return true;
  elseif ($var == null) return true;
  elseif ($var == "")   return true;
  // elseif ($var == " ") return true;
  else return false;
}

/* ******************************** *
  *  Получение IP пользователя
* ********************************* */

function getUserIP() {
  $client  = @$_SERVER['HTTP_CLIENT_IP'];
  $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
  $remote  = @$_SERVER['REMOTE_ADDR'];

  if (filter_var($client, FILTER_VALIDATE_IP)) $ip = $client;
  elseif (filter_var($forward, FILTER_VALIDATE_IP)) $ip = $forward;
  else $ip = $remote;

  return $ip;
}

/* ******************************** *
  *  Сканирование папки
* ********************************* */

function isJson($string) {
  json_decode($string);
  return (json_last_error() == JSON_ERROR_NONE);
}

/* ******************************** *
  *  Сканирование папки
* ********************************* */

function compressContent($data) {
  $data = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $data);
  $data = str_replace(["\r\n", "\r", "\n", "\t", '  ', '    ', '    '], '', $data);
  return $data;
}

/* ******************************** *
  *  Сканер папки
* ********************************* */

function scandirs($start, $deep = false) {
  static $current_level = 0;
  $files = array();
  $handle = opendir($start);
  while (false !== ($file = readdir($handle))) {
    if ($file != '.' && $file != '..') {
      if (is_dir($start . '/' . $file)) {
        if ($current_level === $deep) {
          continue;
        }
        $current_level++;
        $dir = scandirs($start . '/' . $file, $deep);
        $files[$file] = $dir;
      } else {
        array_push($files, $file);
      }
    }
  }
  closedir($handle);
  $current_level--;
  return $files;
}

/* ******************************** *
  *  Получить права файла или папки
* ********************************* */

function perms($filename) {
	return substr(decoct(fileperms($filename)), -3);
}

/* ******************************** *
  *  Округление в большую сторону
  *  до числа кратного $x
* ********************************* */

function roundUp($n,$x=5) {
  return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
}

/* ******************************** *
  *  Округление в меньшую сторону
  *  до числа кратного $x
* ********************************* */

function roundDown($n,$x=5) {
  return (ceil($n)%$x === 0) ? ceil($n) : round(($n-$x/2)/$x)*$x;
}

/* ******************************** *
  *  Округление в меньшую сторону
  *  до числа кратного $x
* ********************************* */

function randString($length = 16) {
  $chars = 'abdefhiknrstyzABDEFHIKNRSTYZ0123456789';
  $numChars = strlen($chars);
  $string = '';
  for ($i = 0; $i < $length; $i++) {
    $string .= substr($chars, rand(1, $numChars) - 1, 1);
  }
  return $string;
}

/* ******************************** *
  *  Конвертирование массива в код
* ********************************* */

function convertArrayToCode($array, $var = 'array', $_livel = null) {
  $out = $margin ='';
  $nr  = "\n";
  $tab = "  ";

  if (is_null($_livel)) {
    if (!empty($array)) $out .= convertArrayToCode($array, $var, 0);
    $out .= ';';
  } else {
    for ($n = 1; $n <= $_livel; $n++) $margin .= $tab;
    $_livel++;

    if (is_array($array)) {
      $i = 1;
      $count = count($array);
      $out .= '[' . $nr;
      foreach ($array as $key => $row) {
        $out .= $margin . $tab;
        if (!is_numeric($key)) $out .= "'" . $key . "' => ";

        if (is_array($row)) $out .= convertArrayToCode($row, $var, $_livel);
        elseif (is_null($row)) $out .= 'null';
        elseif (is_numeric($row)) $out .= $row;
        else $out .= "'" . addslashes($row) . "'";

        if ($count > $i) 	$out .= ',';

        $out .= $nr;
        $i++;
      }

      $out .= $margin . ']';
    } else {
      $out .= "'" .  addslashes($array) . "'";
    }
  }

  return $out;
}

/* ******************************** *
  *  Конвертирование массива в код
* ********************************* */

function translit($string) {
  $string = (string)$string; // преобразуем в строковое значение
  $string = strip_tags($string); // убираем HTML-теги
  $string = str_replace(["\n", "\r"], " ", $string); // убираем перевод каретки
  $string = preg_replace("/\s+/", ' ', $string); // удаляем повторяющие пробелы
  $string = trim($string); // убираем пробелы в начале и конце строки
  $string = function_exists('mb_strtolower') ? mb_strtolower($string) : strtolower($string); // переводим строку в нижний регистр (иногда надо задать локаль)
  $string = strtr($string, ['а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>'']);
  $string = preg_replace("/[^0-9a-z-_ ]/i", "", $string); // очищаем строку от недопустимых символов
  $string = str_replace(" ", "-", $string); // заменяем пробелы знаком минус
  return $string; // возвращаем результат
}

/* ******************************** *
  *  Конвертация абсолютного пути
  *  в относительный
  *  (избавляемся от ROOT_DIR)
* ********************************* */

function formatLink($url) {
  $url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $url);
  $url = str_replace(ROOT_DIR, '', $url);
  return $url;
}


/* ******************************** *
  *  Проверка изображения
  * ********************************* */

function checkImage($url, $replace = true, $urlReplace = ASSETS_DIR . "/images/no-image.jpg") {
  $return = (@file_exists($url)) ? true : false;

  if ($replace == true) {
    if ($return) $return = $url;
    else $return = $urlReplace;
  }

  return $return;
}

/* ******************************** *
  *  Форматирование даты
  * ********************************* */

function formatDate($date, $time = true) {
  $month = date("%m", $date);
  $currentDate = (int) date('d', $date) . date(' %m Y', $date);
  $currentDate = str_replace($month, LNG::getText('month_' . (int) substr($month, 1)), $currentDate);

  if ($time == true) {
    $currentDate .= date(' в H:i', $date);
  }

  return $currentDate;
}

/* ******************************** *
  *  Обрезать строку до N символа
  *  и добавить троеточие
  * ********************************* */

function shorten($string, $length, $ending = '...') {
  // $string = substr($string, 0, $length);
  // $string = rtrim($string, "!,.-");
  // $string = substr($string, 0, strrpos($string, ' '));

  $len = strlen(trim($string));
  $string = (($len > $length) && ($len != 0)) ? rtrim(mb_strimwidth($string, 0, $length - strlen($ending))) . $ending : $string;
  return $string;
}

/* ******************************** *
  *  Форматирование HTML
  * ********************************* */

function formatHTML($string) {
  $string = strip_tags($string);
  return $string;
}

/* ******************************** *
  *  Форматирование HTML
  * ********************************* */

function getTemplateName() {
  return CFG::get('theme');
}

/* ******************************** *
  *  Форматирование HTML
  * ********************************* */

function getTemplatePath() {
  return TEMPLATES_DIR . '/' . getTemplateName();
}

/* ******************************** *
  *  Форматирование HTML
  * ********************************* */

function getTemplateURI() {
  return str_replace(ROOT_DIR, '', getTemplatePath());
}

/* ******************************** *
  *  Форматирование HTML
  * ********************************* */

function getPath($dir, $relative = false) {
  $paths = [
    'assets' => ASSETS_DIR,
    'template' => TEMPLATES_DIR . '/' . getTemplateName(),
    'templates' => TEMPLATES_DIR,
  ];
  return ($relative == true) ? str_replace(ROOT_DIR, '', $paths[$dir]) : $paths[$dir];
}

/* ******************************** *
  *  Маркер выхода
  * ********************************* */

function marker() {
  d('MARKER (' . formatDate(time()) . ')');
}