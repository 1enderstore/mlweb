var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

var Paths = {
  CSS: './mlweb_designs/templates/minilife/styles/css',
  SCSS_TOOLKIT_SOURCES: './mlweb_designs/templates/minilife/styles/scss/init.scss',
  SCSS: './mlweb_designs/templates/minilife/styles/scss/**/**'
};

gulp.task('compile-scss', function() {
  return gulp.src(Paths.SCSS_TOOLKIT_SOURCES)
    // .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    // .pipe(autoprefixer())
    .pipe(gulp.dest(Paths.CSS));
});

gulp.task('watch', function() {
  gulp.watch(Paths.SCSS, gulp.series('compile-scss'));
});
