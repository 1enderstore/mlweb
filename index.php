<?php
/* **************************************************************** *
  *  MiniLife - Your little life...
  *  Назначение:
  *    Подгрузка всей CMS
* ***************************************************************** */

@error_reporting(E_ALL);
@ini_set("display_errors", 1);
@ini_set('html_errors', 1);

define('ROOT_DIR', __DIR__);
define('CORE_DIR', ROOT_DIR . '/mlweb_core');

require_once CORE_DIR . '/init.php';
