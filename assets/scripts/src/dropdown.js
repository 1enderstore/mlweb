(function ($) {
  'use strict';

  var defaults = {};

  function Menu(element, options) {
    this.$el = $(element);
    this.opt = $.extend(true, {}, defaults, options);

    this.init(this);
  }

  function openMenu(self) {
    self.show()

    setTimeout(function() {
      self.addClass('dropdown-opened');
    }, 1);
  }

  function closeMenu(self) {
    self.removeClass('dropdown-opened');

    setTimeout(function() {
      self.hide()
    }, 200);
  }

  Menu.prototype = {
    init: function (self) {
      $('.dropdown').on('hover', function (e) {
        var $target = $(e.target);
        var $this = $(this);

        $this.toggleClass('dropdown-opened');

        var $dropdown = $this.children(".dropdown-menu");

        if ($dropdown.hasClass('dropdown-opened')) {
          closeMenu($dropdown);
        } else {
          openMenu($dropdown);
        }

        // if ($target.closest(self.$el.data('dropdown-toggle'))[0]) {
        //   $target = $target.closest(self.$el.data('dropdown-toggle'));
        //   console.log("$target", $target)

        //   // self.$el
        //   //   // .css(self.calcPosition($target))
        //   //   // .css({display: 'block'})
        //   //   .toggleClass('dropdown-opened');

        //   if (self.$el.hasClass('dropdown-opened')) {
        //     closeMenu(self.$el);
        //   } else {
        //     openMenu(self.$el);
        //   }

        //   e.preventDefault();
        // } else if (!$target.closest(self.$el)[0]){
        //   closeMenu(self.$el);
        // }
      });
    },

    calcPosition: function ($target) {
      var windowWidth, targetOffset, position;

      windowWidth = $(window).width();
      targetOffset = $target.offset();

      position = {
        top: targetOffset.top + ($target.outerHeight() / 2)
      };

      if (targetOffset.left > windowWidth / 2) {
        this.$el
          .addClass('menu--right')
          .removeClass('menu--left');

        position.right = (windowWidth - targetOffset.left) - ($target.outerWidth() / 2);
        position.left = 'auto';
      } else {
        this.$el
          .addClass('menu--left')
          .removeClass('menu--right');

        position.left = targetOffset.left + ($target.outerWidth() / 2);
        position.right = 'auto';
      }

      return position;
    }
  };

  $.fn.dropdown = function (options) {
    return this.each(function() {
      if (!$.data(this, 'menu')) {
        $.data(this, 'menu', new Menu(this, options));
      }
    });
  };
})(window.jQuery);
