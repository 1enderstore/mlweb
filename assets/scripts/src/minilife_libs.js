function isEmpty(str) {
  if (str != null && typeof str !== "undefined")
    str = str.trim();
  if (!str) return true;

  return false;
}

function d(text, type, color) {
  if (type == 'info') {
    if (color) console.info('%c ' + text + ' ', 'background: #222; color: #5af42c');
    else console.info(text);
  }
}

function reloadConent() {
  $.pjax.reload('#content');
}

function unAuth() {
  $.ajax({
      url: '/api/user.unauth',
      type: 'GET',
      dataType: 'json',
      data: "username=" + _data['username'],
    })
    .done(function($response) {
      if ($response['code'] == 2) {
        new Noty({
          text: $response['response']['message'],
          message: 'Message',
          type: 'success',
        }).show();
      } else {
        new Noty({
          text: $response['response']['message'],
          message: 'Message',
          type: 'warning',
        }).show();
      }
    })
    .fail(function() {
      new Noty({
        text: 'Fuck login',
        message: 'Message',
        type: 'error',
      }).show();
    });
}

function setLocation(curLoc) {
  location.href = curLoc;
  location.hash = curLoc;
}


function autoLoader($time = 0.3) {
  $('#loading_icon').fadeIn(300);
  $('#loading_icon').delay($time * 1000).fadeOut(300);
}

function showLoader() {
  $('#loading_icon').fadeIn(300);
}

function hideLoader() {
  $('#loading_icon').delay(300).fadeOut(300);
}

$(document).ready(function() {
  d('Page loaded successfully!', 'info', true);
});