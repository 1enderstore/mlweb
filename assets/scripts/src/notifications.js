/**
 * Анонимная самовызывающаяся функция-обертка
 * @param {document} d - получает документ
 */
! function(d) {

  "use strict";

  /**
   * Основная функция.
   * @param {Object} [settings] - предвартиельные настройки
   */
  window.note = function(settings) {

    /**
     * Настройки по умолчанию
     */
    settings = Object.assign({}, {
      callback: false,
      content: "",
      time: 4.5,
      type: "info"
    }, settings);

    if (!settings.content.length) return;

    /**
     * Функция создания элементов
     * @param {String} name - название DOM-элемента
     * @param {Object} attr - объект с атрибутами
     * @param {Object} append - DOM-элемент, в который будет добавлен новый узел
     * @param {String} [content] - контент DOM-элемента
     */
    var create = function(name, attr, append, content) {
      var node = d.createElement(name);
      for (var val in attr) {
        if (attr.hasOwnProperty(val)) node.setAttribute(val, attr[val]);
      }
      if (content) node.insertAdjacentHTML("afterbegin", content);
      append.appendChild(node);
      if (node.classList.contains("note-item-hidden")) node.classList.remove("note-item-hidden");
      return node;
    };

    /**
     * Генерация элементов
     */
    var noteBox = d.getElementById("notes") || create("div", {
      "id": "notes"
    }, d.body);
    var noteItem = create("div", {
        "class": "note-item",
        "data-show": "false",
        "role": "alert",
        "data-type": settings.type
      }, noteBox),
      noteItemText = create("div", {
        "class": "note-item-text"
      }, noteItem, settings.content),
      noteItemBtn = create("span", {
        "class": "note-item-btn",
        "type": "button",
        "aria-label": "Hidden"
      }, noteItem);

    /**
     * Функция проверки видимости алерта во viewport
     * @returns {boolean}
     */
    var isVisible = function() {
      var coords = noteItem.getBoundingClientRect();
      return (
        coords.top >= 0 &&
        coords.left >= 0 &&
        coords.bottom <= (window.innerHeight || d.documentElement.clientHeight) &&
        coords.right <= (window.innerWidth || d.documentElement.clientWidth)
      );
    };

    /**
     * Функция удаления алертов
     * @param {Object} [el] - удаляемый алерт
     */
    var remove = function(el) {
      el = el || noteItem;
      el.setAttribute("data-show", "false");
      window.setTimeout(function() {
        el.remove();
      }, 250);
      if (settings.callback) settings.callback(); // callback
    };

    /**
     * Удаление алерта по клику на кнопку
     */
    noteItemBtn.addEventListener("click", function() {
      remove();
    });

    /**
     * Визуальный вывод алерта
     */
    window.setTimeout(function() {
      noteItem.setAttribute("data-show", "true");
    }, 250);


    /**
     * Проверка видимости алерта и очистка места при необходимости
     */
    if (!isVisible()) remove(noteBox.firstChild);

    /**
     * Автоматическое удаление алерта спустя заданное время
     */
    window.setTimeout(remove, settings.time * 1000);

  };

}(document);

function notice($content, $type = "info", $time = 5, $collback = false) {
  note({
    callback: $collback,
    content: $content,
    type: $type,
    time: $time
  });
}




/* -------------------------------------------------------------------------- */



'use strict';

var Notice = function(element, config) {
  var
    _this = this,
    _element = element,
    _config = {
      autohide: true,
      delay: 5000
    };
  for (var prop in config) {
    _config[prop] = config[prop];
  }
  Object.defineProperty(this, 'element', {
    get: function() {
      return _element;
    }
  });
  Object.defineProperty(this, 'config', {
    get: function() {
      return _config;
    }
  });
  _element.addEventListener('click', function(e) {
    if (e.target.classList.contains('notice_close')) {
      _this.hide();
    }
  });
}

Notice.prototype = {
  show: function() {
    var _this = this;
    this.element.classList.add('notice_show');
    if (this.config.autohide) {
      setTimeout(function() {
        _this.hide();
      }, this.config.delay)
    }
  },
  hide: function() {
    var event = new CustomEvent('hidden.notice', {
      detail: {
        toast: this.element
      }
    });
    this.element.classList.remove('notice_show');
    document.dispatchEvent(event);
  }
};

Notice.create = function(header, body, type) {
  var fragment = document.createDocumentFragment();

  var noticeBody = document.createElement('div');
  noticeBody.classList.add('notice');
  noticeBody.classList.add('notice-' + type);
  noticeBody.classList.add('add-margin');

  var noticeIcon = document.createElement('div');
  noticeBody.appendChild(noticeIcon);
  noticeIcon.classList.add('notice_icon');

  var noticeContent = document.createElement('div');
  noticeBody.appendChild(noticeContent);
  noticeContent.classList.add('notice_content');

  var noticeTitle = document.createElement('p');
  noticeContent.appendChild(noticeTitle);
  noticeTitle.classList.add('notice_type');
  noticeTitle.textContent = header;

  var noticeMessage = document.createElement('p');
  noticeContent.appendChild(noticeMessage);
  noticeMessage.classList.add('notice_message');
  noticeMessage.textContent = body;

  var noticeClose = document.createElement('div');
  noticeBody.appendChild(noticeClose);
  noticeClose.classList.add('notice_close');

  fragment.appendChild(noticeBody);
  return fragment;
};

// Notice.create = function (header, body, color) {
//   var
//     fragment = document.createDocumentFragment(),
//     toast = document.createElement('div'),
//     toastHeader = document.createElement('div'),
//     toastClose = document.createElement('button'),
//     toastBody = document.createElement('div');
//   toast.classList.add('toast');
//   toast.style.backgroundColor = 'rgba(' + parseInt(color.substr(1, 2), 16) + ',' + parseInt(color.substr(3, 2), 16) + ',' + parseInt(color.substr(5, 2), 16) + ',0.5)';
//   toastHeader.classList.add('toast__header');
//   toastHeader.textContent = header;
//   toastClose.classList.add('toast__close');
//   toastClose.setAttribute('type', 'button');
//   toastClose.textContent = '×';
//   toastBody.classList.add('toast__body');
//   toastBody.textContent = body;
//   toastHeader.appendChild(toastClose);
//   toast.appendChild(toastHeader);
//   toast.appendChild(toastBody);
//   fragment.appendChild(toast);
//   return fragment;
// };

Notice.add = function(params) {
  var config = {
    title: 'Название заголовка',
    message: 'Текст сообщения...',
    type: 'info',
    autohide: true,
    delay: 2000
  };
  if (params !== undefined) {
    for (var item in params) {
      config[item] = params[item];
    }
  }
  if (!document.querySelector('.notices')) {
    var container = document.createElement('div');
    container.classList.add('notices');

    var notice_container = document.createElement('div');
    container.appendChild(notice_container);
    notice_container.classList.add('notice_container');

    var notice_cell = document.createElement('div');
    notice_container.appendChild(notice_cell);
    notice_cell.classList.add('notice_cell');

    document.body.appendChild(container);
  }
  document.querySelector('.notice_cell').appendChild(Notice.create(config.title, config.message, config.type));
  var notices = document.querySelectorAll('.notice_cell');
  var toast = new Notice(notices[notices.length - 1], {
    autohide: config.autohide,
    delay: config.delay
  });
  toast.show();
  return toast;
}

document.addEventListener('hidden.toast', function(e) {
  var element = e.detail.toast;
  if (element) {
    element.parentNode.removeChild(element);
  }
});

function notice(title, message, type, delay) {
  Notice.add({
    title: title,
    message: message,
    type: type,
    delay: delay
  })
}